/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.service;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.music.bean.MusicInfo;
import com.ingenic.music.util.AppConstant;
import com.ingenic.music.util.IntentConstant;
import com.ingenic.music.util.MediaUtil;
import com.ingenic.music.util.PreferencesConstant;

/**
 * 2015/7/29
 *
 * 音乐播放服务
 *
 * @author MingDan
 * 
 */
public class PlayerService extends Service {
	/**
	 * 媒体播放器对象
	 */
	private MediaPlayer mediaPlayer;

	/**
	 * 音乐文件路径
	 */
	private String path;

	/**
	 * 播放控制信息
	 */
	private int msg;

	/**
	 * 记录当前正在播放的音乐
	 */
	private int current = 0;

	/**
	 * 存放MusicInfo对象的集合
	 */
	private ArrayList<MusicInfo> musicInfos;

	/**
	 * 播放状态，默认为列表循环播放
	 */
	private int status = MODE_REPEAT_ALL;

	/**
	 * 自定义广播接收器
	 */
	private MyReceiver myReceiver;

	/**
	 * 当前播放进度
	 */
	private int currentTime;

	private SharedPreferences preferences;

	/**
	 * 记录选中的播放的音乐文件的类型
	 */
	private int type_music_file = LOCAL_MUSIC_FILE;

	/**
	 * mp3音乐文件类型
	 */
	private static final int LOCAL_MUSIC_FILE = 0;

	/**
	 * 录音文件类型
	 */
	private static final int RECORDER_MUSIC_FILE = 1;

	/**
	 * 定义三个播放模式的常量
	 */
	private static final int MODE_REPEAT_ONE = 1;
	private static final int MODE_REPEAT_ALL = 2;
	private static final int MODE_SHUFFLE = 3;

	private static final int REFRESH_UI = 1;

	/**
	 * 录音文件的后缀名字符串常量
	 */
	private static final String RECORDER_FILE_EXTENSION = "amr";

	/**
	 * handler用来接收消息，来发送广播更新播放时间
	 */
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == REFRESH_UI) {
				if ((mediaPlayer != null) && mediaPlayer.isPlaying()) {
					currentTime = mediaPlayer.getCurrentPosition(); // 获取当前音乐播放的位置

					Intent intent = new Intent();
					intent.setAction(AppConstant.MUSIC_CURRENT);
					intent.putExtra(IntentConstant.currentTime, currentTime);
					sendBroadcast(intent); // 给PlayingActivity和Widget发送广播,更新UI
					handler.sendEmptyMessageDelayed(REFRESH_UI, 1000);
				}
			}
		};
	};

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		mediaPlayer = new MediaPlayer();
		preferences = getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName, Context.MODE_PRIVATE);
		//musicInfos = getmMusicInfos();

		// 还原状态
		type_music_file = preferences.getInt(PreferencesConstant.typeMusicFlie,
				LOCAL_MUSIC_FILE);
		status = preferences.getInt(PreferencesConstant.mode, MODE_REPEAT_ALL);
		
		preferences.edit().putBoolean(PreferencesConstant.is_service_die, false).commit();

		// 提高服务的级别，降低服务被android系统杀死的概率
		CharSequence label = getApplication().getApplicationInfo().loadLabel(
				getPackageManager());

		Notification notification = new Notification(getApplication()
				.getApplicationInfo().icon, label, System.currentTimeMillis());

		Intent it = new Intent(ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
		it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
		notification.setLatestEventInfo(this, label, "", pendingIntent);
		notification.flags = notification.flags
				| Notification.FLAG_ONGOING_EVENT;

		startForeground(9998, notification);

		/**
		 * 设置音乐播放完成时的监听器
		 */
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				if (musicInfos == null) {
					return;
				}

				if (status == MODE_REPEAT_ONE) { // 单曲循环
					mediaPlayer.start();

				} else if (status == MODE_REPEAT_ALL) { // 全部循环
					current++;

					if (current > (musicInfos.size() - 1)) { // 变为第一首的位置继续播放
						if(musicInfos.size() > 0) {
							current = 0;
						}
					}

					path = musicInfos.get(current).url;

					Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
					sendIntent.putExtra(IntentConstant.current, current);

					// 发送广播，将被Activity组件中的BroadcastReceiver接收到
					sendBroadcast(sendIntent);

					play(0);

				} else if (status == MODE_SHUFFLE) { // 随机播放
					int oldposition = current;
					current = getRandomIndex(musicInfos.size() - 1);

					if (current == oldposition) {
						current = (current + 1) % musicInfos.size();
					}
					path = musicInfos.get(current).url;

					Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
					sendIntent.putExtra(IntentConstant.current, current);

					// 发送广播，将被Activity组件中的BroadcastReceiver接收到
					sendBroadcast(sendIntent);

					play(0);
				}
				// 发送广播，更新widget UI
				Intent intent_change_ui = new Intent();
				intent_change_ui.setAction(AppConstant.CHANGE_WIDGET_UI);
				intent_change_ui.putExtra(IntentConstant.current, current);
				intent_change_ui.putExtra(IntentConstant.typeMusicFile, type_music_file);
				sendBroadcast(intent_change_ui);
			}

		});

		myReceiver = new MyReceiver();

		IntentFilter filter = new IntentFilter();
		filter.addAction(AppConstant.CTL_ACTION);
		filter.addAction(AppConstant.PROGRESS_CHANGE);
		filter.addAction(AppConstant.ALARM_BELL_STOP_MUSIC);
		filter.addAction(AppConstant.ALARM_STOP_MUSIC_START);
		filter.addAction(AppConstant.RECORDER_STOP_MUSIC);
		filter.addAction(AppConstant.RECORDER_STOP_MUSIC_START);
		filter.addAction(AppConstant.PREVIOUS_MUSIC);
		filter.addAction(AppConstant.NEXT_MUSIC);
		filter.addAction(AppConstant.PAUSE_RESUME);
		filter.addAction(AppConstant.ACTION_INCOMING_CALL);
		filter.addAction(AppConstant.ACTION_OUT_CALL);
		filter.addAction(AppConstant.ACTION_STOP_CALL);
		filter.addAction(AppConstant.SERVICE_GET_MUSICLIST);
		registerReceiver(myReceiver, filter);
	}

	/**
	 * 获取随机位置
	 *
	 * @param end
	 * @return
	 */
	protected int getRandomIndex(int end) {
		int index = (int) (Math.random() * end);

		return index;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			path = intent.getStringExtra(IntentConstant.url); // 歌曲路径
			current = intent.getIntExtra(IntentConstant.listPosition, -1); // 当前播放歌曲的位置
			msg = intent.getIntExtra(IntentConstant.playerControlMsg, 0); // 播放信息

			type_music_file = intent.getIntExtra(IntentConstant.typeMusicFile,LOCAL_MUSIC_FILE); // 播放音乐文件的类型
			musicInfos = getmMusicInfos();
			//musicInfos = (ArrayList<MusicInfo>) intent.getSerializableExtra("listobj");

			if (msg == AppConstant.PlayerMsg.PLAY_MSG) { // 直接播放音乐
				play(0);
			} else if (msg == AppConstant.PlayerMsg.PAUSE_MSG) { // 暂停
				pause();
			} else if (msg == AppConstant.PlayerMsg.STOP_MSG) { // 停止
				stop();
			} else if (msg == AppConstant.PlayerMsg.CONTINUE_MSG) { // 继续播放
				resume();
			} else if (msg == AppConstant.PlayerMsg.PRIVIOUS_MSG) { // 上一首
				previous();
			} else if (msg == AppConstant.PlayerMsg.NEXT_MSG) { // 下一首
				next();
			} else if (msg == AppConstant.PlayerMsg.PLAYING_MSG) {
				handler.sendEmptyMessage(REFRESH_UI);
			}

			preferences.edit().putBoolean(PreferencesConstant.isOk, true).commit();

			// 发送广播，更新widget UI
			Intent intent_change_ui = new Intent();
			intent_change_ui.setAction(AppConstant.CHANGE_WIDGET_UI);
			intent_change_ui.putExtra(IntentConstant.current, current);
			intent_change_ui.putExtra(IntentConstant.typeMusicFile, type_music_file);
			sendBroadcast(intent_change_ui);

			return super.onStartCommand(intent, flags, startId);

		} else {
			return 0;
		}
	}

	/**
	 * 播放音乐
	 *
	 * @param position
	 */
	private void play(int currentTime) {
		try {
			File file = new File(path);
			//播放时如果音乐文件不存在了，就进行自动下一曲的播放
			if (!file.exists()) {
				current++;

				if (current > (musicInfos.size() - 1)) { // 变为第一首的位置继续播放
					if(musicInfos.size() > 0) {
						current = 0;
					}
				}

				path = musicInfos.get(current).url;

				Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
				sendIntent.putExtra(IntentConstant.current, current);

				// 发送广播，将被Activity组件中的BroadcastReceiver接收到
				sendBroadcast(sendIntent);

				play(0);
				//return;
			}

			mediaPlayer.reset(); // 把各项参数恢复到初始状态
			mediaPlayer.setDataSource(path);
			mediaPlayer.prepare(); // 进行缓冲
			mediaPlayer
					.setOnPreparedListener(new PreparedListener(currentTime)); // 注册一个监听器
			handler.sendEmptyMessage(REFRESH_UI);
		} catch (Exception e) {
			e.printStackTrace();
		}

		preferences.edit().putInt(PreferencesConstant.currentPosition, current)
				.putString(PreferencesConstant.currentUrl, path)
				.putBoolean(PreferencesConstant.isPlaying, true).commit();
	}

	/**
	 * 暂停音乐
	 */
	private void pause() {
		if ((mediaPlayer != null) && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			preferences.edit().putBoolean(PreferencesConstant.isPlaying, false)
					.commit();
		}
	}

	/**
	 * 继续播放
	 */
	private void resume() {
		mediaPlayer.start();
		if (mediaPlayer.isPlaying()) {
			handler.sendEmptyMessage(1);
			preferences.edit().putBoolean(PreferencesConstant.isPlaying, true)
					.commit();
		} else { // 异常情况下需要重新加载这首歌曲进行播放
			play(0);
		}
	}

	/**
	 * 上一首
	 */
	private void previous() {
		Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
		sendIntent.putExtra(IntentConstant.current, current);
		sendBroadcast(sendIntent);
		play(0);
	}

	/**
	 * 下一首
	 */
	private void next() {
		Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
		sendIntent.putExtra(IntentConstant.current, current);
		sendBroadcast(sendIntent);
		play(0);
	}

	/**
	 * 停止音乐
	 */
	private void stop() {
		if (mediaPlayer != null) {
			mediaPlayer.stop();

			try {
				mediaPlayer.prepare(); // 在调用stop后如果需要再次通过start进行播放,需要之前调用prepare函数
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		if (myReceiver != null) {
			unregisterReceiver(myReceiver);
		}

		if (mediaPlayer != null) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
			preferences.edit()
					//.putInt(PreferencesConstant.currentPosition, -1)
					.putBoolean(PreferencesConstant.isPlaying, false).commit();
		}

		if (musicInfos != null) { // 手动将musicInfos置为空，回收掉
			musicInfos.clear();
			musicInfos = null;
		}
		preferences.edit().putBoolean(PreferencesConstant.is_service_die, true).commit();
		sendBroadcast(new Intent(AppConstant.PLAYER_SERVICE_DIE));
	}

	/**
	 * 实现一个OnPrepareLister接口,当音乐准备好的时候开始播放
	 */
	private final class PreparedListener implements OnPreparedListener {
		private int currentTime;

		public PreparedListener(int currentTime) {
			this.currentTime = currentTime;
		}

		@Override
		public void onPrepared(MediaPlayer mp) {
			mediaPlayer.start(); // 开始播放

			if (currentTime > 0) { // 如果音乐不是从头播放
				mediaPlayer.seekTo(currentTime);
			}
		}
	}

	public class MyReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(musicInfos == null) {
				return;
			}
			
			String action = intent.getAction();

			if (action.equals(AppConstant.CTL_ACTION)) {
				int control = intent.getIntExtra(IntentConstant.control, -1);

				switch (control) {
				case MODE_REPEAT_ONE:
					status = MODE_REPEAT_ONE; // 将播放状态置为1表示：单曲循环

					break;

				case MODE_REPEAT_ALL:
					status = MODE_REPEAT_ALL; // 将播放状态置为2表示：全部循环

					break;

				case MODE_SHUFFLE:
					status = MODE_SHUFFLE; // 将播放状态置为3表示：随机播放

					break;
				}
			} else if (action.equals(AppConstant.PROGRESS_CHANGE)) {
				if (mediaPlayer != null) {
					path = intent.getStringExtra(IntentConstant.url); // 歌曲路径
					current = intent.getIntExtra(IntentConstant.listPosition,
							-1); // 当前播放歌曲

					int currentProgress = intent.getIntExtra(
							IntentConstant.currentProgress, -1); // 当前播放歌曲的最新进度
					// 从currentProgress位置开始播放
					play(currentProgress);
				}
			} else if (action.equals(AppConstant.ALARM_BELL_STOP_MUSIC)) { // 闹钟响铃时暂停音乐
				IwdsLog.d("PlayerService", "ALARM_BELL_STOP_MUSIC");
				pause();
			} else if (action.equals(AppConstant.ALARM_STOP_MUSIC_START)) { // 闹钟结束时继续播放
				if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
					IwdsLog.d("PlayerService", "ALARM_STOP_MUSIC_START");
					mediaPlayer.start();
					handler.sendEmptyMessage(1);
					preferences.edit()
							.putBoolean(PreferencesConstant.isPlaying, true)
							.commit();
				}
			}  else if (action.equals(AppConstant.RECORDER_STOP_MUSIC)) { // 录音机播放录音时暂停音乐
				IwdsLog.d("PlayerService", "RECORDER_STOP_MUSIC");
				pause();
			} else if (action.equals(AppConstant.RECORDER_STOP_MUSIC_START)) { // 录音机播放录音结束时继续播放
				if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
					IwdsLog.d("PlayerService", "RECORDER_STOP_MUSIC_START");
					mediaPlayer.start();
					handler.sendEmptyMessage(REFRESH_UI);
					preferences.edit()
							.putBoolean(PreferencesConstant.isPlaying, true)
							.commit();
				}
			} else if (action.equals(AppConstant.ACTION_INCOMING_CALL) || action.equals(AppConstant.ACTION_OUT_CALL)) { // 来电或外拨电话时暂停音乐
				IwdsLog.d("PlayerService", "INCOMING_CALL_OR_OUT_CALL_STOP_MUSIC");
				pause();
			} else if (action.equals(AppConstant.ACTION_STOP_CALL)) { // 电话挂断音乐继续播放
				if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
					IwdsLog.d("PlayerService", "CALL_STOP_MUSIC_START");
					mediaPlayer.start();
					handler.sendEmptyMessage(REFRESH_UI);
					preferences.edit()
							.putBoolean(PreferencesConstant.isPlaying, true)
							.commit();
				}
			}else if (action.equals(AppConstant.PREVIOUS_MUSIC)) {
				musicInfos = getmMusicInfos();

				current--;

				if (current < 0) { // 变为最后一首的位置继续播放
					current = musicInfos.size() - 1;
				}

				path = musicInfos.get(current).url;

				Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
				sendIntent.putExtra(IntentConstant.current, current);
				preferences
				.edit()
				.putBoolean(PreferencesConstant.isPlaying, true)
				.commit();
				// 发送广播，将被Activity组件中的BroadcastReceiver接收到
				sendBroadcast(sendIntent);
				play(0);
				// 发送广播，更新widget UI
				Intent intent_change_ui = new Intent();
				intent_change_ui.setAction(AppConstant.CHANGE_WIDGET_UI);
				intent_change_ui.putExtra(IntentConstant.current, current);
				intent_change_ui.putExtra(IntentConstant.typeMusicFile, type_music_file);
				sendBroadcast(intent_change_ui);
			} else if (action.equals(AppConstant.NEXT_MUSIC)) {
				musicInfos = getmMusicInfos();
				current++;

				if (current > musicInfos.size() - 1) { // 变为第一首的位置继续播放
					if(musicInfos.size() > 0) {
						current = 0;
					}
				}

				path = musicInfos.get(current).url;

				Intent sendIntent = new Intent(AppConstant.UPDATE_ACTION);
				sendIntent.putExtra(IntentConstant.current, current);
				// 发送广播，将被Activity组件中的BroadcastReceiver接收到
				sendBroadcast(sendIntent);
				play(0);
				// 发送广播，更新widget UI
				Intent intent_change_ui = new Intent();
				intent_change_ui.setAction(AppConstant.CHANGE_WIDGET_UI);
				intent_change_ui.putExtra(IntentConstant.current, current);
				intent_change_ui.putExtra(IntentConstant.typeMusicFile, type_music_file);
				sendBroadcast(intent_change_ui);
				preferences
				.edit()
				.putBoolean(PreferencesConstant.isPlaying, true)
				.commit();
				Log.i("mingdan", "next......"+current);
			} else if (action.equals(AppConstant.PAUSE_RESUME)) {
				Log.i("mingdan", "PAUSE_RESUME......");
				if (preferences
						.getBoolean(PreferencesConstant.isPlaying, false)) {
					preferences.edit()
							.putBoolean(PreferencesConstant.isPlaying, false)
							.commit();
					pause();
					Log.i("mingdan", "pause.......");
				} else {
					Log.i("mingdan", "mediaPlayer:"+mediaPlayer);
					Log.i("mingdan", "mediaPlayer.isPlaying():"+mediaPlayer.isPlaying());
					if (mediaPlayer != null/* && !mediaPlayer.isPlaying()*/) {
						mediaPlayer.start();
						//handler.sendEmptyMessage(REFRESH_UI);
						preferences
								.edit()
								.putBoolean(PreferencesConstant.isPlaying, true)
								.commit();
						Log.i("mingdan", "resume......");
					}
				}
				// 发送广播，更新widget UI
				//Intent intent_change_ui = new Intent();
				//intent_change_ui.setAction(AppConstant.CHANGE_WIDGET_UI);
				//intent_change_ui.putExtra(IntentConstant.current, current);
				//intent_change_ui.putExtra(IntentConstant.typeMusicFile, type_music_file);
				//sendBroadcast(intent_change_ui);
				sendBroadcast(new Intent(AppConstant.PAUSE_RESUME_CHANGE_WIDGET_UI));
			} else if(action.equals(AppConstant.SERVICE_GET_MUSICLIST)) {
				//刷新完毕重新获取musicInfos
				musicInfos = getmMusicInfos();
			}
		}
	}

	/**
	 * 获取分类的播放列表
	 *
	 * @return
	 */
	private ArrayList<MusicInfo> getmMusicInfos() {
		musicInfos = MediaUtil.getMusicInfos(PlayerService.this);
		ArrayList<MusicInfo> mMp3Infos = new ArrayList<MusicInfo>();
		ArrayList<MusicInfo> mRecorderInfos = new ArrayList<MusicInfo>();
		for (MusicInfo info : musicInfos) {
			String fileExtension = MediaUtil.getFileExtension(new File(info.displayName));
			if (fileExtension.equals(RECORDER_FILE_EXTENSION)) {
				mRecorderInfos.add(info);
			} else {
				mMp3Infos.add(info);
			}
		}

		if (type_music_file == LOCAL_MUSIC_FILE) {
			musicInfos = mMp3Infos;
		} else if (type_music_file == RECORDER_MUSIC_FILE) {
			musicInfos = mRecorderInfos;
		}
		return musicInfos;
	}
}
