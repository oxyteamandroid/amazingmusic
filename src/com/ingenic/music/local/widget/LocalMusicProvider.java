/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.local.widget;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import com.ingenic.music.R;
import com.ingenic.music.bean.MusicInfo;
import com.ingenic.music.ui.MusicListActivity;
import com.ingenic.music.util.AppConstant;
import com.ingenic.music.util.IntentConstant;
import com.ingenic.music.util.MediaUtil;
import com.ingenic.music.util.PreferencesConstant;
import com.ingenic.music.util.Utils;

/**
 * 桌面本地音乐的Widget
 * @author MingDan
 *
 */
public class LocalMusicProvider extends AppWidgetProvider {
	private RemoteViews mRemoteViews;
	private SharedPreferences preferences;

    /**
     * view的点击事件
     */
    public static final String VIEW_CLICK_ACTION = "widget_view_click_action";
    /**
     * 音量变化的广播
     */
    public static final String VOLUME_CHANGE_ACTION = "android.media.VOLUME_CHANGED_ACTION";
	
	private AudioManager mAudioManager;
	private int maxVolume;
	private int currentVolume;
	
	/**
     * 音量控制比率常量
     */
    private static final int VOLUMN_RATIO = 5;
	
	/**
     * 录音文件的后缀名字符串常量
     */
    private static final String RECORDER_FILE_EXTENSION = "amr";

    /**
     * 播放的音乐的类型：0代表本地音乐文件，1代表录音文件
     */
    private int typeMusicFile;
    
    /**
	 * mp3音乐文件类型
	 */
	private static final int LOCAL_MUSIC_FILE = 0;

	/**
	 * 录音文件类型
	 */
	private static final int RECORDER_MUSIC_FILE = 1;

    /**
     * 当前播放音乐的下标
     */
    private int currentPosition;

    private boolean isServiceDie = true;

    /**
     * 音乐媒体库中所有的音乐文件集合
     */
    private ArrayList<MusicInfo> musicInfos;
    
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);
	}

	@SuppressLint("NewApi")
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		initAttrs(context, appWidgetManager);
		setOnViewClick(context, R.id.widget_info);
		initViews(context);
		// 設置一系列点击事件
		// 过滤所有的快速点击事件
		if (Utils.isFastClick())
			return;
		setOnViewClick(context, R.id.widget_previous_music);
		setOnViewClick(context, R.id.widget_next_music);
		setOnViewClick(context, R.id.playBtn);
		setOnViewClick(context, R.id.ll_decrease);
		setOnViewClick(context, R.id.ll_add);
       //更新widget
       updateAllWidgets(context);
	}

	private void initAttrs(Context context, AppWidgetManager appWidgetManager) {
		preferences = context.getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName, Context.MODE_PRIVATE);

		typeMusicFile = preferences
				.getInt(PreferencesConstant.typeMusicFlie, 0);
		currentPosition = preferences.getInt(
				PreferencesConstant.currentPosition, -1);
		isServiceDie = preferences.getBoolean(
				PreferencesConstant.is_service_die, true);

		musicInfos = getMusicInfos(context, typeMusicFile);

		getRemoteViews(context);
	}
	private void initViews(Context context) {
		if (musicInfos == null
				|| (musicInfos != null && musicInfos.size() <= 0)) {
			mRemoteViews.setTextViewText(R.id.widget_title,
					context.getString(R.string.no_music));
			return;
		}

		if (isServiceDie) {
			mRemoteViews.setTextViewText(R.id.widget_title,
					context.getString(R.string.local_music));
			return;
		}

		// 初始化播放按钮图片
		setPlayState(preferences);
		showTitle(currentPosition, musicInfos);
		//设置音量
		initVolume(context);
		setProgressBar(maxVolume, currentVolume);
	}

	private RemoteViews getRemoteViews(Context context) {
		if (Utils.IsCircularScreen()) {
			mRemoteViews = new RemoteViews(context.getPackageName(),
					R.layout.naturalround_widget_music);
			Log.i("mingdan", "圆屏");
		} else {
			mRemoteViews = new RemoteViews(context.getPackageName(),
					R.layout.wisesquare_widget_music);
		}

		return mRemoteViews;
	}

    private void setOnViewClick(Context context, int viewId) {
        Intent intent = new Intent(VIEW_CLICK_ACTION);
        intent.setClass(context, LocalMusicProvider.class);
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        intent.setData(Uri.parse("custom:" + viewId));
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(context, 0,intent, 1);
        mRemoteViews.setOnClickPendingIntent(viewId, mPendingIntent);
    }

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		String action = intent.getAction();
		mRemoteViews = getRemoteViews(context);
		preferences = context.getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName,
				Context.MODE_PRIVATE);
		if (AppConstant.CHANGE_WIDGET_UI.equals(action)) {
			currentPosition = intent.getIntExtra(IntentConstant.current, -1);
			typeMusicFile = intent.getIntExtra(IntentConstant.typeMusicFile, 0);

			musicInfos = getMusicInfos(context, typeMusicFile);
			showTitle(currentPosition, musicInfos);
			mRemoteViews.setImageViewResource(R.id.playBtn, R.drawable.music_widget_pause);
			updateAllWidgets(context);
		} else if (AppConstant.PAUSE_RESUME_CHANGE_WIDGET_UI.equals(action)){
			setPlayState(preferences);
			updateAllWidgets(context);
		} else if (intent.hasCategory(Intent.CATEGORY_ALTERNATIVE)) {
			Uri data = intent.getData();
			int viewId = Integer.parseInt(data.getSchemeSpecificPart());
			setClickEvent(context, viewId);
		} else if (VOLUME_CHANGE_ACTION.equals(action)) {
			//设置音量
			initVolume(context);
			setProgressBar(maxVolume, currentVolume);
			updateAllWidgets(context);
		} else if(AppConstant.PLAYER_SERVICE_DIE.equals(action)) {
			//后台播放服务死掉了
			mRemoteViews.setTextViewText(R.id.widget_title,context.getString(R.string.local_music));
			mRemoteViews.setTextViewText(R.id.widget_duration,"");
			Log.i("mingdan", "server die......");
			updateAllWidgets(context);
		}
	}

	private void showTitle(int currentPosition,ArrayList<MusicInfo> musicInfos) {
		if (currentPosition >= 0 && currentPosition < musicInfos.size()){
			mRemoteViews.setTextViewText(R.id.widget_title,musicInfos.get(currentPosition).title);
			if(!"<unknown>".equals(musicInfos.get(currentPosition).artist)) {
				mRemoteViews.setTextViewText(R.id.widget_duration,musicInfos.get(currentPosition).artist);
			} else {
				mRemoteViews.setTextViewText(R.id.widget_duration,"");
			}
		}
	}
	private void setPlayState(SharedPreferences preferences) {
		if (preferences != null && !preferences
				.getBoolean(PreferencesConstant.isPlaying, false)) {
			mRemoteViews.setImageViewResource(R.id.playBtn, R.drawable.music_widget_play);
		} else {
			mRemoteViews.setImageViewResource(R.id.playBtn, R.drawable.music_widget_pause);
		}
	}
	private void initVolume(Context context) {
		mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC); // 获取系统最大音量
		currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC); // 获取当前值
	}
	/**
     * Updates the widget when something changes, or when a button is pushed.
     *
     * @param context
     */
    private void updateAllWidgets(Context context) {
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        appWidgetManager.updateAppWidget(new ComponentName(context,LocalMusicProvider.class), mRemoteViews);
    }

	private void setClickEvent(Context context, int clickId) {
		switch (clickId) {
		case R.id.widget_info:
			Intent mIntent = new Intent(context, MusicListActivity.class);
			mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(mIntent);
			break;

		case R.id.widget_previous_music:
			context.sendBroadcast(new Intent(AppConstant.PREVIOUS_MUSIC));
			break;

		case R.id.widget_next_music:
			context.sendBroadcast(new Intent(AppConstant.NEXT_MUSIC));
			break;

		case R.id.playBtn:
			context.sendBroadcast(new Intent(AppConstant.PAUSE_RESUME));
			break;

		case R.id.ll_add:
			initVolume(context);
			currentVolume = currentVolume + maxVolume / VOLUMN_RATIO;
			if (currentVolume > maxVolume) {
				currentVolume = maxVolume;
			}
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
					currentVolume, 0);
			setProgressBar(maxVolume, currentVolume);
			updateAllWidgets(context);
			break;

		case R.id.ll_decrease:
			initVolume(context);
			currentVolume = currentVolume - maxVolume / VOLUMN_RATIO;
			if (currentVolume < 0) {
				currentVolume = 0;
			}
			mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
					currentVolume, 0);
			setProgressBar(maxVolume, currentVolume);
			updateAllWidgets(context);
			break;

		default:
			break;
		}
	}
	
	private void setProgressBar(int maxVolume, int currentVolume) {
		mRemoteViews.setProgressBar(R.id.volume_seekbar, maxVolume, currentVolume, false);
	}

	/**
	 * 获取指定类型的音乐列表集合
	 * @param context
	 * @param typeMusicFile
	 * @return
	 */
	private ArrayList<MusicInfo> getMusicInfos(Context context,int typeMusicFile) {
		ArrayList<MusicInfo> musicInfos = MediaUtil.getMusicInfos(context);
		ArrayList<MusicInfo> mp3Infos = new ArrayList<MusicInfo>();
		ArrayList<MusicInfo> recorderInfos = new ArrayList<MusicInfo>();

		if (mp3Infos != null)
			mp3Infos.clear();

		if (recorderInfos != null)
			recorderInfos.clear();

		for (MusicInfo info : musicInfos) {
			String fileExtension = MediaUtil.getFileExtension(new File(
					info.displayName));
			if (fileExtension.equals(RECORDER_FILE_EXTENSION))
				recorderInfos.add(info);
			else
				mp3Infos.add(info);
		}
		
		if (typeMusicFile == LOCAL_MUSIC_FILE) {
			musicInfos = mp3Infos;
		} else if (typeMusicFile == RECORDER_MUSIC_FILE) {
			musicInfos = recorderInfos;
		}
		return musicInfos;
	}
}
