/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.music.remote.widget;

/**
 * RemoteMusicConstatns
 * 
 * @author Shi Guanghua
 * 
 */
public class RemoteMusicConstatns {
    /**
     * 远程音乐服务的uuid
     */
    public static String REMOTE_MUSIC_UUID = "B0569AA5-53BE-F627-F7E9-2F3012920595";

    /**
     * 手表端控制手机端音乐的指令action
     */
    public static final String COM_MUSIC_SEND_ACTION = "com.music.send.command";

    /**
     * 远程音乐的传递tag
     */
    public static final String SET_MUSIC_INFO = "com.ingenic.music.info";

    /**
     * 重启音乐服务的action
     */
    public static final String RESTART_MUSIC_SERVICE_ACTION = "com.restart.music.service.action";

    /**
     * 远程音乐改变的action
     */
    public static final String COM_INGENIC_MUSIC_ACTION = "com.ingenic.music.changed";

    /**
     * 通道是否畅通action
     */
    public static final String ACTION_CHANNEL_AVAILABLE = "com.ingenic.channel.avaiable";

    /**
     * remote music view的点击事件
     */
    public static final String REMOTE_MUSIC_VIEW_CLICK_ACTION = "music_remote_view_click_action";

    /**
     * 当前音量 命令
     */
    public static final int VOLUME_CURRENT = -5;

    /**
     * 音乐改变 命令
     */
    public static final int PLAY_MUSIC_STATE = -1;

    /**
     * 播放
     */
    public static final int REMOTE_MUSIC_START = -1;

    /**
     * 暂停
     */
    public static final int REMOTE_MUSIC_PAUSE = -2;

    /**
     * 下一曲
     */
    public static final int REMOTE_MUSIC_NEXT = -3;

    /**
     * 上一曲
     */
    public static final int REMOTE_MUSIC_PREV = -4;

    /**
     * 远程音乐的信息
     */
    public static final String REMOTE_MUSIC_INFO = "musicInfo";

    /**
     * 通道是否畅通信息
     */
    public static final String CHANNEL_AVAILABLE_INFO = "avaiable";
}
