/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  foror-fighter/elf/watch-apps/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.music.remote.widget;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.MusicControlInfo;
import com.ingenic.iwds.datatransactor.elf.MusicControlTransactionModel;
import com.ingenic.iwds.datatransactor.elf.MusicControlTransactionModel.MusicControlTransactionModelCallback;

/**
 * 远程音乐服务
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class RemoteWidgetMusicService extends Service implements
        MusicControlTransactionModelCallback {

    private Context mContext;
    private static MusicControlTransactionModel mMusicControlTransactionModel = null;
    private CommandChangeReceiver mCommandChangeReceiver;
    private boolean mMusicAvaible = false;
    public static int mVolumeMax, mVolumeCurrent;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @SuppressWarnings({ "deprecation" })
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());
        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());
        mContext = this;
        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;
        startForeground(9999, notification);

        // 初始化数据传输模型
        if (mMusicControlTransactionModel == null) {
            mMusicControlTransactionModel = new MusicControlTransactionModel(
                    mContext, this, RemoteMusicConstatns.REMOTE_MUSIC_UUID);
        }
        mMusicControlTransactionModel.start();
        mCommandChangeReceiver = new CommandChangeReceiver();
        // 注册发送指令的广播
        IntentFilter filter = new IntentFilter();
        filter.addAction(RemoteMusicConstatns.COM_MUSIC_SEND_ACTION);
        this.registerReceiver(mCommandChangeReceiver, filter);
    }

    public class CommandChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (RemoteMusicConstatns.COM_MUSIC_SEND_ACTION.equals(action)) {
                if (intent != null) {
                    Bundle mBundle = intent.getExtras();
                    if (mBundle != null) {
                        MusicControlInfo musicControlInfo = (MusicControlInfo) mBundle
                                .get(RemoteMusicConstatns.SET_MUSIC_INFO);
                        if (musicControlInfo != null && mMusicAvaible)
                            mMusicControlTransactionModel
                                    .send(musicControlInfo);
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMusicControlTransactionModel != null)
            mMusicControlTransactionModel.stop();

        mContext.unregisterReceiver(mCommandChangeReceiver);

        // 重启远程音乐服务
        sendBroadcast(new Intent(
                RemoteMusicConstatns.RESTART_MUSIC_SERVICE_ACTION));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        int resultCode = result.getResultCode();
        switch (resultCode) {
        case DataTransactResult.RESULT_OK:
            // 获取手表端发送的命令，告诉手机端正在处理数据
            break;

        case DataTransactResult.RESULT_FAILED_IWDS_CRASH:
        case DataTransactResult.RESULT_FAILED_LINK_DISCONNECTED:
            break;
        case DataTransactResult.RESULT_FAILED_CHANNEL_UNAVAILABLE:
            // 与手机端链接出现问题，远程音乐不可点击
            break;

        default:
            break;
        }
    }

    @Override
    public void onRequestFailed() {
        // 手表端发送消息失败
    }

    @Override
    public void onRequest() {
    }

    @Override
    public void onObjectArrived(MusicControlInfo musicInfo) {
        // 手表端接受到手机端发过来的数据
        if (musicInfo != null) {
            MusicControlInfo musicControlInfo = musicInfo;
            if (musicControlInfo != null) {
                if (musicControlInfo.cmd == -5) {
                    mVolumeMax = musicControlInfo.volumeMax;
                    mVolumeCurrent = musicControlInfo.volumeCurrent;
                }
                Intent intent = new Intent();
                intent.setAction(RemoteMusicConstatns.COM_INGENIC_MUSIC_ACTION);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("musicInfo", musicControlInfo);
                intent.putExtras(mBundle);
                mContext.sendBroadcast(intent);
            }
        }
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        // 手机端和手表端的链接通路正常
        Intent mIntent = new Intent();
        mIntent.setAction(RemoteMusicConstatns.ACTION_CHANNEL_AVAILABLE);
        if (isAvailable) {
            // 手机端和手表端链路正常，可以操作手机端的远程音乐
            mIntent.putExtra("avaiable", true);
            mContext.sendBroadcast(mIntent);
            mMusicAvaible = true;
        } else {
            mIntent.putExtra("avaiable", false);
            mContext.sendBroadcast(mIntent);
            mMusicAvaible = false;

        }
    }

}
