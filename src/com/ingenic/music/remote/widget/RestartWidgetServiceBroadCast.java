/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingLauncher Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.music.remote.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 重启音乐服务
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class RestartWidgetServiceBroadCast extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (RemoteMusicConstatns.RESTART_MUSIC_SERVICE_ACTION.equals(action))
            context.startService(new Intent(context,
                    RemoteWidgetMusicService.class));
    }
}
