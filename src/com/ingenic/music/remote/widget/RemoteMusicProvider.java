/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  elf/AmazingLauncher project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.music.remote.widget;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RemoteViews;

import com.ingenic.iwds.datatransactor.elf.MusicControlInfo;
import com.ingenic.music.R;
import com.ingenic.music.util.Utils;

/**
 * 
 * @author ShiGuangHua(Kenny)
 * 
 */
public class RemoteMusicProvider extends AppWidgetProvider {

    private static int maxVolume = 15;
    private static int sCurrentVolume;
    private static final int VOLUMN_RATIO = 15;
    private static final int VOLUME_CHANGE_FLAG_MESSAGE = 0x01;

    /**
     * 默认为暂停的状态
     */
    private static boolean mIsPlaying = false;

    private static boolean mMusicChannelAvaible = false;

    private RemoteViews mRemoteViews;
    private MusicControlInfo mMusicControlInfo = null;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {
        mRemoteViews = getRemoteViews(context);
        // 设置按钮的点击事件
        setOnViewClick(context, R.id.ll_play);
        setOnViewClick(context, R.id.iv_previous);
        setOnViewClick(context, R.id.iv_next);
        setOnViewClick(context, R.id.ll_volume_add);
        setOnViewClick(context, R.id.ll_volume_decrease);

        for (int i = 0; i < appWidgetIds.length; i++) {
            appWidgetManager.updateAppWidget(appWidgetIds[i], mRemoteViews);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private void setOnViewClick(Context context, int viewId) {
        Intent intent = new Intent(
                RemoteMusicConstatns.REMOTE_MUSIC_VIEW_CLICK_ACTION);
        intent.setClass(context, RemoteMusicProvider.class);
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        intent.setData(Uri.parse("custom:" + viewId));
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, 1);
        mRemoteViews.setOnClickPendingIntent(viewId, mPendingIntent);
    }

    private RemoteViews getRemoteViews(Context context) {
        if (Utils.IsCircularScreen())
            mRemoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_remote_music_naturalround);
        else
            mRemoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_remote_music_wisesquare);
        return mRemoteViews;
    }

    /**
     * Updates the widget when something changes, or when a button is pushed.
     * 
     * @param context
     */
    public void updateAllWidgets(Context context) {
        final AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        appWidgetManager.updateAppWidget(new ComponentName(context,
                RemoteMusicProvider.class), mRemoteViews);
    }

    @Override
    public void onEnabled(Context context) {
        // 启动服务
        context.startService(new Intent(context, RemoteWidgetMusicService.class));
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        // 停止服务
        context.stopService(new Intent(context, RemoteWidgetMusicService.class));
        super.onDisabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        mRemoteViews = getRemoteViews(context);

        if (RemoteMusicConstatns.COM_INGENIC_MUSIC_ACTION.equals(action)) {
            mMusicControlInfo = (MusicControlInfo) intent.getExtras().get(
                    RemoteMusicConstatns.REMOTE_MUSIC_INFO);
            updateViewState(context, mMusicControlInfo);
            updateAllWidgets(context);
        } else if (RemoteMusicConstatns.ACTION_CHANNEL_AVAILABLE.equals(action)) {
            mMusicChannelAvaible = intent.getExtras().getBoolean(
                    RemoteMusicConstatns.CHANNEL_AVAILABLE_INFO, false);
        } else if (intent.hasCategory(Intent.CATEGORY_ALTERNATIVE)) {
            // 控件的点击事件
            Uri data = intent.getData();
            int viewId = Integer.parseInt(data.getSchemeSpecificPart());
            setClickEvent(context, viewId);
            updateAllWidgets(context);
        }

        super.onReceive(context, intent);
    }

    private void setClickEvent(Context context, int clickId) {
        switch (clickId) {
        case R.id.ll_play:
            if (mMusicChannelAvaible) {
                mMusicControlInfo = new MusicControlInfo();
                if (mIsPlaying) {
                    mMusicControlInfo.cmd = RemoteMusicConstatns.REMOTE_MUSIC_PAUSE;
                } else {
                    mMusicControlInfo.cmd = RemoteMusicConstatns.REMOTE_MUSIC_START;
                }
                controlMusic(context, mMusicControlInfo);
            }
            break;
        case R.id.iv_previous:
            if (mMusicChannelAvaible) {
                mMusicControlInfo = new MusicControlInfo();
                mMusicControlInfo.cmd = RemoteMusicConstatns.REMOTE_MUSIC_PREV;
                controlMusic(context, mMusicControlInfo);
            }

            break;
        case R.id.iv_next:
            if (mMusicChannelAvaible) {
                mMusicControlInfo = new MusicControlInfo();
                mMusicControlInfo.cmd = RemoteMusicConstatns.REMOTE_MUSIC_NEXT;
                controlMusic(context, mMusicControlInfo);
            }
            break;
        case R.id.ll_volume_add:
            if (mMusicChannelAvaible) {
                sCurrentVolume = sCurrentVolume + maxVolume / VOLUMN_RATIO;
                if (sCurrentVolume > maxVolume) {
                    sCurrentVolume = maxVolume;
                }
                controlMobileVolume(context, sCurrentVolume);
            }
            break;
        case R.id.ll_volume_decrease:
            if (mMusicChannelAvaible) {
                sCurrentVolume = sCurrentVolume - maxVolume / VOLUMN_RATIO;
                if (sCurrentVolume < 0) {
                    sCurrentVolume = 0;
                }
                controlMobileVolume(context, sCurrentVolume);
            }
            break;
        default:
            break;
        }
    }

    private void updateViewState(Context context,
            MusicControlInfo musicControlInfo) {
        int cmd = musicControlInfo.cmd;
        if (cmd == RemoteMusicConstatns.VOLUME_CURRENT) {
            maxVolume = musicControlInfo.volumeMax;
            sCurrentVolume = musicControlInfo.volumeCurrent;
            updateVolumeView(maxVolume, sCurrentVolume);
        } else {

            if (cmd != RemoteMusicConstatns.PLAY_MUSIC_STATE) {
                // -1表示 play 播放
                mRemoteViews.setImageViewResource(R.id.iv_play,
                        R.drawable.widget_music_play);
                mIsPlaying = false;
            } else if (cmd == RemoteMusicConstatns.PLAY_MUSIC_STATE) {
                mRemoteViews.setImageViewResource(R.id.iv_play,
                        R.drawable.widget_music_pause);
                mIsPlaying = true;
            }
            // 歌曲名称
            String songName = mMusicControlInfo.songName;
            mRemoteViews.setTextViewText(R.id.tv_remote_music_song_name,
                    songName);

        }

    }

    private void updateVolumeView(int maxVolumes, int currentVolumes) {
        // 更新音量
        mRemoteViews.setProgressBar(R.id.tv_volume_seekbar, maxVolumes,
                currentVolumes, false);
    }

    /**
     * 控制音量
     * 
     * @param Volume
     */
    private void controlMobileVolume(Context context, int Volume) {
        Message msg = mHandler.obtainMessage();
        msg.arg1 = Volume;
        msg.what = VOLUME_CHANGE_FLAG_MESSAGE;
        msg.obj = context;
        mHandler.removeMessages(VOLUME_CHANGE_FLAG_MESSAGE);
        mHandler.sendMessage(msg);
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Context context = null;
            switch (msg.what) {
            case VOLUME_CHANGE_FLAG_MESSAGE:
                mMusicControlInfo = new MusicControlInfo();
                mMusicControlInfo.cmd = RemoteMusicConstatns.VOLUME_CURRENT;
                int currentVolume = msg.arg1;
                context = (Context) msg.obj;
                updateVolumeView(maxVolume, currentVolume);
                updateAllWidgets(context);
                mMusicControlInfo.volumeCurrent = currentVolume;
                controlMusic(context, mMusicControlInfo);
                break;

            default:
                break;
            }

        }
    };

    /**
     * 控制音乐
     * 
     * @param musicControlInfo
     */
    private void controlMusic(Context context, MusicControlInfo musicControlInfo) {
        Intent intent = new Intent(RemoteMusicConstatns.COM_MUSIC_SEND_ACTION);
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(RemoteMusicConstatns.SET_MUSIC_INFO,
                musicControlInfo);
        intent.putExtras(mBundle);
        context.sendBroadcast(intent);
    }

}
