/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.bean;


/**
 * 2015/7/29 Music实体类
 * 
 * @author MingDan
 * 
 */
public class MusicInfo {
	
	/**
	 * 歌曲ID 3
	 */
	public long id; 
	
	/**
	 * 歌曲名称 0
	 */
	public String title; 
	
	/**
	 * 专辑 7
	 */
	public String album; 
	
	/**
	 * 专辑ID 6
	 */
	public long albumId;
	
	/**
	 * 显示名称 4
	 */
	public String displayName; 
	
	/**
	 * 歌手名称 2
	 */
	public String artist; 
	
	/**
	 * 歌曲时长 1
	 */
	public long duration; 
	
	/**
	 * 歌曲大小 8
	 */
	public long size; 
	
	/**
	 * 歌曲路径 5
	 */
	public String url; 

	@Override
	public String toString() {
		return "MusicInfo [id=" + id + ", title=" + title + ", album=" + album
				+ ", albumId=" + albumId + ", displayName=" + displayName
				+ ", artist=" + artist + ", duration=" + duration + ", size="
				+ size + ", url=" + url + "]";
	}

}
