/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.view;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v4.widget.ViewDragHelper.Callback;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.LinearLayout;

public class SwipeLayout extends LinearLayout {

	private ViewDragHelper dragHelper;

	View leftView;
	View rightView;
	int rightWidth;

	private OnSwipeListener osl;
	
	public boolean isEnabled = true;

	public interface OnSwipeListener {
		void onOpen(SwipeLayout swipeLayout, boolean open);

		void onDraging(SwipeLayout swipeLayout, float percent);

	}

	public SwipeLayout(Context context) {
		this(context, null);
	}

	public SwipeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		dragHelper = ViewDragHelper.create(this, callback);
		gestureDetector = new GestureDetector(getContext(),
				new GestureDetector.SimpleOnGestureListener() {

					@Override
					public boolean onScroll(MotionEvent e1, MotionEvent e2,
							float distanceX, float distanceY) {
						return Math.abs(distanceX) > Math.abs(distanceY);
					}
								
					@Override
					public boolean onSingleTapUp(MotionEvent e) {
						if(isEnabled) {
							performAdapterViewItemClick(e);  
							return true;
						}
						return false;
					}
					
					@Override
					public boolean onSingleTapConfirmed(MotionEvent e) {
						return false;
					}	

				});
	}
	
	private void performAdapterViewItemClick(MotionEvent e) {
			ViewParent t = getParent();
			
			while (t != null) {
				if (t instanceof AdapterView) {
					@SuppressWarnings("rawtypes")
					AdapterView view = (AdapterView) t;
					int p = view.getPositionForView(SwipeLayout.this);
					if (p != AdapterView.INVALID_POSITION
							&& view.performItemClick(
									view.getChildAt(p-view.getFirstVisiblePosition()), p,
									view.getAdapter().getItemId(p)))
						return;
				} else {
					if (t instanceof View && ((View) t).performClick())
						return;
				}
				t = t.getParent();
			}
		}

	public void setOnSwipeListener(OnSwipeListener osl) {
		this.osl = osl;
	}

	// 2 将触摸事件交由ViewDragHelper去拦截
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (dragHelper.shouldInterceptTouchEvent(ev)) {
			return true;
		} else {
			return super.onInterceptTouchEvent(ev);
		}
	}


	
	// 3 将触摸事件交由ViewDragHelper处理
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// 如果发现是横向的拖动，则要求父控件不要来拦截触摸事件。
		if(gestureDetector.onTouchEvent(event)){
			requestDisallowInterceptTouchEvent(true);
		}
		
		
		try {
			dragHelper.processTouchEvent(event);
		} catch (Exception e) {

		}
		return true;
	}

	private Callback callback = new Callback() {
		// 告诉ViewDragHelper，child可以被拖拽
		@Override
		public boolean tryCaptureView(View child, int pointerId) {

			return child == leftView || child == rightView;
		}

		@Override
		public int getViewHorizontalDragRange(View child) {
			return 1;
		}

		// 将要移动的位置，child的left边的位置
		// 限制移动范围
		@Override
		public int clampViewPositionHorizontal(View child, int left, int dx) {
			//@formatter:off
			if(child ==  leftView){
				left  = left > 0             ?  0              // leftView的left边不能大于0
					  : left  < - rightWidth ?  -rightWidth	   // leftView的left边不能小于-右边view的宽度
					  : left;
			}else if(child ==  rightView){
				left  = left > getWidth()              ?  getWidth()             // rightView的left边不能大于整个控件的宽度
					  : left < getWidth() - rightWidth ?  getWidth() -rightWidth // rightView的left边不能小于整个控件的宽度-自身的宽度
					  : left;
			}
			//@formatter:on
			return left;	//
		}

		@Override
		public void onViewPositionChanged(View changedView, int left, int top,
				int dx, int dy) {
			if (changedView == leftView) {
				rightView.offsetLeftAndRight(dx);
			} else if (changedView == rightView) {
				leftView.offsetLeftAndRight(dx);
			}
			// 因为rightView在一开始的时候，在屏幕外边，所以系统没有绘制它
			// 有时候，拖出来的时候，rightView没有显示，但能够拖动，
			// 我们进行强制刷新

			if (osl != null) {
				osl.onDraging(SwipeLayout.this, 1.0f * leftView.getLeft()
						/ rightWidth);
			}

			invalidate();

		}

		@Override
		public void onViewReleased(View releasedChild, float xvel, float yvel) {
			if (xvel > 200) {
				close();
			} else if (xvel < -200) {
				open();
			} else if (leftView.getLeft() < -rightWidth / 2) {
				open();
			} else {
				close();
			}
		}

		@Override
		public void onViewDragStateChanged(int state) {
			if (state == ViewDragHelper.STATE_IDLE) {
				if (osl != null) {
					// == 0的时候是关闭状态
					osl.onOpen(SwipeLayout.this, leftView.getLeft() != 0);
				}

			}
		}

	};

	private GestureDetector gestureDetector;

	public  void close() {
		if (dragHelper.smoothSlideViewTo(leftView, 0, 0)) {
			// 传leftView的父控件
			ViewCompat.postInvalidateOnAnimation(this);
		}

	}

	private void open() {
		if (dragHelper.smoothSlideViewTo(leftView, -rightWidth, 0)) {
			// 传leftView的父控件
			ViewCompat.postInvalidateOnAnimation(this);
		}
	}

	@Override
	public void computeScroll() {
		if (dragHelper.continueSettling(true)) {
			ViewCompat.postInvalidateOnAnimation(this);
		}
	}

	protected void onFinishInflate() {

		leftView = getChildAt(0);
		rightView = getChildAt(1);

	};

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {

		super.onSizeChanged(w, h, oldw, oldh);

		if (rightWidth == 0) {
			rightWidth = rightView.getMeasuredWidth();
		}
	}

}
