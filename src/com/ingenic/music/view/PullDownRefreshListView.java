/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingenic.music.R;

/**
 * 2015/7/29
 * 
 * 自定义控件：PullDownRefreshListView 下拉刷新ListView
 * 
 * @author MingDan
 *
 */
public class PullDownRefreshListView extends ListView {
	
	private ImageView iv_arrow;				// 箭头 
    private ProgressBar pb;					// 进度条 
    private TextView tv_refresh;			// 刷新状态文本 
    private int viewHeaderMeasuredHeight;	// 测量后的高度 
    private int myFirstVisibleItem;		// listView中第一个可见的条目 
    private float downY = -1;				// 触摸按下Y轴的坐标，防止其他事件，导致触摸事件没有响应 
    private float moveY;						// 触摸移动时Y轴的坐标 
    private View viewHeader;				// 头布局 
     
    private HeaderDisplayMode headerDisplayMode = HeaderDisplayMode.PULL_DOWN;		//headerView的状态，默认为下拉刷新 
    private float paddingTop;						// headerView的上边距 
    private Animation refresh_down_animation;		// 箭头向下动画 
    private Animation refresh_up_animation;		// 箭头向上动画 
    private OnRefreshListener mOnRefreshListener;// 刷新的监听器


    public PullDownRefreshListView(Context context, AttributeSet attrs) { 
        super(context, attrs); 
        initHeader();// 初始化头 
    } 
    public PullDownRefreshListView(Context context) { 
        super(context); 
        initHeader();// 初始化头 
    } 
    /** 
     * 初始化头布局 
     */ 
    public void initHeader() { 
        viewHeader = View.inflate(getContext(), R.layout.listview_head, null); 
        iv_arrow = (ImageView) viewHeader.findViewById(R.id.iv_arrow); 
        pb = (ProgressBar) viewHeader.findViewById(R.id.pb); 
        tv_refresh = (TextView) viewHeader.findViewById(R.id.tv_refresh_status); 
         
        measureHeader(viewHeader);// 测量高度，否则获取不到头布局高度 
        viewHeaderMeasuredHeight = viewHeader.getMeasuredHeight(); 
         
        viewHeader.setPadding(0, -viewHeaderMeasuredHeight, 0, 0);//隐藏头布局 
        addHeaderView(viewHeader); 
         
        // 注册滚动监听事件 
        this.setOnScrollListener(new MyOnScrollListener()); 
        // 初始化动画 
        intiAnimation(); 
        
    } 
    
    
    /** 
     * 初始化动画 
     */ 
    private void intiAnimation() { 
         
        // 代码定义动画 
        refresh_down_animation = new RotateAnimation(-180, -360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f); 
        refresh_down_animation.setDuration(500); 
        refresh_down_animation.setFillAfter(true);
        
        refresh_up_animation = new RotateAnimation(0, -180,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f); 
        refresh_up_animation.setDuration(500); 
        refresh_up_animation.setFillAfter(true);
    } 
    
    /** 
     * 测量并计算head的宽度和高度 测量item,这是在ListView中私有的方法，所以可以将其黏贴过来  
     * Measure a particular list child. unify with setUpChild. 
     * @param viewHeaderThe child. 
     */ 
    private void measureHeader(View viewHeader) { 
        ViewGroup.LayoutParams lp = viewHeader.getLayoutParams(); 
        if (lp == null) { 
            lp = new ViewGroup.LayoutParams( 
                    ViewGroup.LayoutParams.MATCH_PARENT, 
                    ViewGroup.LayoutParams.WRAP_CONTENT); 
        } 
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, lp.width);// 测量标准 
        int lpHeight = lp.height; 
        int childHeightSpec; 
        if (lpHeight > 0) { 
            childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight, MeasureSpec.EXACTLY); 
        } else { 
            childHeightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED); 
        } 
        viewHeader.measure(childWidthSpec, childHeightSpec); 
    } 
         
    /** 
     * 滚动事件-获取第一个item在ListView中的位置 
     * @author MingDan
     */ 
    private class MyOnScrollListener implements OnScrollListener { 
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			myFirstVisibleItem = firstVisibleItem; 
			
		} 
    } 
   
	private int downCount = -1;	//用于计数被向下拉下的次数
	
    // 触摸事件 
    @Override 
    public boolean onTouchEvent(MotionEvent event) { 
          
        switch (event.getAction()) { 
        case MotionEvent.ACTION_DOWN:// 触摸按下 
            downY = (int) event.getY();// downY初始值为-1，防止其他事件，导致触摸事件没有响应 
            break; 
        case MotionEvent.ACTION_MOVE:// 触摸移动 
            // 如果当前的状态是正在刷新中, 跳出 
            if (headerDisplayMode == HeaderDisplayMode.ISREFRESHING) {// 正在刷新时，不可触摸移动 
                if (downY == -1) { 
                    downY = (int) event.getY(); 
                  } 
                  // 获得移动中y轴的偏移量 
                moveY = event.getY(); 
                  // 头布局的负数 + (移动中y轴的偏移量 - 按下时y轴的偏移量) / 2 
                paddingTop = -viewHeaderMeasuredHeight + (moveY - downY) / 2; 
                viewHeader.setPadding(0, (int) paddingTop, 0, 0); 
                break; 
            } 
            
        	
            if (downY == -1) { 
                downY = (int) event.getY(); 
            } 
            // 获得移动中y轴的偏移量 
            moveY = event.getY(); 
            // 头布局的负数 + (移动中y轴的偏移量 - 按下时y轴的偏移量) / 2 
            paddingTop = -viewHeaderMeasuredHeight + (moveY - downY) / 2; 
            //判断paddingTop是否大于头布局的高度的负数，并且滚动时第一个显示的item为0 进入if语句 
            if (myFirstVisibleItem==0 && paddingTop > -viewHeaderMeasuredHeight) {// 开始下拉刷新，如果不判断myFirstVisibleItem，后面的item下拉时也不正常 
                // 开始移动下拉布局 
                if (paddingTop > 50// 完全显示头布局 (为了防止总是进入刷新界面，所以条件加强点)
                        && headerDisplayMode == HeaderDisplayMode.PULL_DOWN) {// 如果大于0，说明已经完全出来了，变成松手刷新状态 
                    // 改变当前的状态为 松开刷新 
                    headerDisplayMode = HeaderDisplayMode.REALEASE_REFRESH; 
                    refreshHeaderView(); 
                } else if (paddingTop <= 50 // 还没有完全显示头布局 
                        && headerDisplayMode == HeaderDisplayMode.REALEASE_REFRESH) {// 小于0，那么变成下拉刷新 
                    // 改变当前的状态为下拉状态 
                    headerDisplayMode = HeaderDisplayMode.PULL_DOWN; 
                    refreshHeaderView(); 
                } 
                // refreshHeaderView();//不能放在这里，如果在松手状态，会多次执行松手动画，导致，箭头上下在移动，影响用户体验 
                viewHeader.setPadding(0, (int) paddingTop, 0, 0); 
                return true;// 消费掉该事件 
            } 
            break; 
        case MotionEvent.ACTION_UP:// 触摸松手 
    	
            // 如果当前状态是正在刷新中 跳出 
            if (headerDisplayMode == HeaderDisplayMode.ISREFRESHING) {// 正在刷新时，不可触摸移动 
            	downCount++;
            	if(downCount%2==0){
            		paddingTop = -viewHeaderMeasuredHeight-50;
            	}else{
            		paddingTop = 0;
            	}
              viewHeader.setPadding(0, (int) paddingTop, 0, 0);          
              break; 
            } 
            
            // 如果当前状态是松开刷新, 进入if 做刷新的操作 
            if (headerDisplayMode == HeaderDisplayMode.REALEASE_REFRESH) { 
            	  downCount = 1;
                headerDisplayMode = HeaderDisplayMode.ISREFRESHING; 
                paddingTop = 0;  
                viewHeader.setPadding(0, (int) paddingTop, 0, 0); 
                refreshHeaderView(); 
                if (mOnRefreshListener != null) {// 如果调用者实现了OnRefreshListener接口 
                    // 调用刷新回调方法 
                    mOnRefreshListener.onRefresh();// 调用接口中暴露出去的方法 
                } 
            // 当下拉状态时 
            } else if (headerDisplayMode == HeaderDisplayMode.PULL_DOWN) { 
                // 把头布局复位为隐藏状态 
                paddingTop = -viewHeaderMeasuredHeight; 
                viewHeader.setPadding(0, (int) paddingTop, 0, 0); 
                refreshHeaderView(); 
            } 
            downY = -1; 
            moveY = -1; 
            break; 
        } 
        return super.onTouchEvent(event);// 消费掉该事件,如果true，那么就消费了该事件，那么就无法滑动了，所以不能为true 
    } 
    
    /** 
     * 刷新头布局 
     */ 
    private void refreshHeaderView() { 
        if (headerDisplayMode == HeaderDisplayMode.PULL_DOWN) {// 下拉状态 
            iv_arrow.startAnimation(refresh_down_animation); 
            tv_refresh.setText(getResources().getString(R.string.refresh_down)); 
        } else if (headerDisplayMode == HeaderDisplayMode.REALEASE_REFRESH) {// 松手刷新 
            iv_arrow.startAnimation(refresh_up_animation); 
            tv_refresh.setText(getResources().getString(R.string.refresh_up)); 
        } else if (headerDisplayMode == HeaderDisplayMode.ISREFRESHING) {// 正在刷新 
            iv_arrow.clearAnimation();// 清理动画 
            iv_arrow.setVisibility(View.GONE); 
            pb.setVisibility(View.VISIBLE); 
            tv_refresh.setText(getResources().getString(R.string.refreshing)); 
        } 
    } 
    
    /** 
     * headerView状态枚举 
     */ 
    public enum HeaderDisplayMode { 
        PULL_DOWN, // 下拉刷新 
        REALEASE_REFRESH, // 松手刷新 
        ISREFRESHING // 正在刷新中 
    } 
     
    /** 
     * 当刷新时的监听事件实现的接口 
     */ 
    public interface OnRefreshListener { 
        public abstract void onRefresh(); 
    } 
    
    
    /** 
     * 设置刷新时监听器 
     * @param mOnRefreshListener 
     */ 
    public void setOnRefreshListener(OnRefreshListener mOnRefreshListener) { 
        this.mOnRefreshListener = mOnRefreshListener; 
    } 
    
    /** 
     * 刷新完成后，将状态初始化为下拉状态 
     */ 
    public void refreshFinish() { 
        // 隐藏头布局 
        viewHeader.setPadding(0, -viewHeaderMeasuredHeight, 0, 0); 
        // 更改头布局状态为下拉 
        headerDisplayMode = HeaderDisplayMode.PULL_DOWN; 
        // 进度条不可见 
        pb.setVisibility(View.GONE); 
        // 箭头可见 
        iv_arrow.setVisibility(View.VISIBLE); 
        // 头布局状态文本 
        tv_refresh.setText(getResources().getString(R.string.refresh_down)); 
    } 
    

}

