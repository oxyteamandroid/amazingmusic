/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * 自定义ImageView:自身能处理显示图片的旋转，暂停旋转,产生动画的效果
 * 
 * @author MingDan
 *
 */

public class RotateImageView extends ImageView{

	protected static final int REFRESH = 0;
	private float degrees = 0;
	/**
	 * 画布每毫秒旋转的角度常量
	 */
	private static final float DEGREES_PER_MILLIS = 0.12f;
	/**
	 * 每隔多长时间发一个刷新消息
	 */
	private static final long MILLIS = 1;
	public RotateImageView(Context context) {
		super(context);
		startRotation();
	}

	public RotateImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		startRotation();
	}

	public RotateImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		startRotation();
	}
	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(msg.what == REFRESH) {
				RotateImageView.this.invalidate();
			}
		};
	};
	/**
	 * 发送更新界面的消息的子线程
	 */
	private Thread mThread = null;
	private boolean start = true;
	private void startRotation() {
		mThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while(start){
					mHandler.sendEmptyMessage(REFRESH);
					/**
					 * 每隔1毫秒，发个消息去旋转画布，重新执行onDraw()方法，刷新界面
					 */
					try {
						Thread.sleep(MILLIS);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		mThread.start();
	}
	@Override
	protected void onDraw(Canvas canvas) {
		degrees += DEGREES_PER_MILLIS;
		/**
		 * 旋转画布
		 */
		canvas.rotate(degrees, getWidth()/2, getHeight()/2);
		super.onDraw(canvas);
	}
	public void pause() {
		if(mThread != null) {
			mThread = null;
		}
		start = false;
	}
	public void resume() {
		start = true;
		startRotation();
	}
}

