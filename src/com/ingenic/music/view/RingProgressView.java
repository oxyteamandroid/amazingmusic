/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsCompatibilityChecker;
import com.ingenic.music.R;
import com.ingenic.music.util.MediaUtil;

/**
 * 自定义控件：环形进度控件
 * 
 * @author MingDan
 * 
 */
public class RingProgressView extends View {

	// 画实心圆的画笔
	private Paint mCirclePaint;
	// 画外圈的画笔
	private Paint mOuterRingPaint;
	// 画圆环的画笔
	private Paint mRingPaint;
	// 圆形颜色
	private int mCircleColor;
	// 圆环颜色
	private int mRingColor;
	// 半径
	private float mCircleRadius;
	// 圆环半径
	private float mRingRadius;
	// 圆与圆环间隔宽度
	private float mSpaceWidth;
	// （外/进度）圆环宽度
	private float mRingWidth;
	// 圆心x坐标
	private int mXCenter;
	// 圆心y坐标
	private int mYCenter;
	// 总进度
	private int mTotalProgress = 100;
	// 当前进度
	private int mProgress;

	public RingProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);

		IwdsAssert.dieIf(this, !IwdsCompatibilityChecker.getInstance().check(),
				"Compatibility check failed.");

		// 获取自定义的属性
		initAttrs(context, attrs);
		initVariable();
	}

	/**
	 * 获取自定义属性值
	 */
	private void initAttrs(Context context, AttributeSet attrs) {
		TypedArray typeArray = context.getTheme().obtainStyledAttributes(attrs,
				R.styleable.RingProgressView, 0, 0);

		// 圆的半径
		mCircleRadius = typeArray.getDimension(
				R.styleable.RingProgressView_circleRadius,
				MediaUtil.dip2px(context, 50));

		// 圆与圆环间隔宽度
		mSpaceWidth = typeArray.getDimension(
				R.styleable.RingProgressView_spaceWidth,
				MediaUtil.dip2px(context, 15));

		// 圆环半径
		mRingRadius = mCircleRadius + mSpaceWidth;

		// 外圈/进度的宽度
		mRingWidth = typeArray.getDimension(
				R.styleable.RingProgressView_ringWidth,
				MediaUtil.dip2px(context, 3));

		mCircleColor = typeArray.getColor(
				R.styleable.RingProgressView_circleAndRingColor, 0x4800DC7C);

		mRingColor = typeArray.getColor(
				R.styleable.RingProgressView_progressRingColor, 0xBBFAFAFA);

	}

	/**
	 * 初始化
	 */
	private void initVariable() {
		mCirclePaint = new Paint();
		mCirclePaint.setAntiAlias(true);
		mCirclePaint.setColor(mCircleColor);
		mCirclePaint.setStyle(Paint.Style.FILL);

		mOuterRingPaint = new Paint();
		mOuterRingPaint.setAntiAlias(true);
		mOuterRingPaint.setColor(mCircleColor);
		mOuterRingPaint.setStyle(Paint.Style.STROKE);
		mOuterRingPaint.setStrokeWidth(5);

		mRingPaint = new Paint();
		mRingPaint.setAntiAlias(true);
		mRingPaint.setColor(mRingColor);
		mRingPaint.setStyle(Paint.Style.STROKE);
		mRingPaint.setStrokeWidth(mRingWidth);
	}

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {

		mXCenter = getWidth() / 2;
		mYCenter = getHeight() / 2;

		canvas.drawCircle(mXCenter, mYCenter, mCircleRadius, mCirclePaint);
		canvas.drawCircle(mXCenter, mYCenter, mRingRadius, mOuterRingPaint);

		if (mProgress >= 0) {
			RectF oval = new RectF();
			oval.left = mXCenter - mRingRadius;
			oval.top = mYCenter - mRingRadius;
			oval.right = mXCenter + mRingRadius;
			oval.bottom = mYCenter + mRingRadius;
			canvas.drawArc(oval, -90,
					((float) mProgress / mTotalProgress) * 360, false,
					mRingPaint);
		}
	}

	public void setProgress(int progress) {
		mProgress = progress;
		postInvalidate();
	}

}
