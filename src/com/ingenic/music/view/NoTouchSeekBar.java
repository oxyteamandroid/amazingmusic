/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.music.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class NoTouchSeekBar extends SeekBar {

	public NoTouchSeekBar(Context context) {
		super(context);
	}

	public NoTouchSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public NoTouchSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	/**
	 * onTouchEvent 是在 SeekBar 继承的抽象类 AbsSeekBar 里 你可以看下他们的继承关系
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// 原来是要将TouchEvent传递下去的,我们不让它传递下去就行了
		// return super.onTouchEvent(event);
		return false;
	}
}
