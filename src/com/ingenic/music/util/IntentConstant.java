/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.util;

public interface IntentConstant {
	
	/**
	 * 要切换到播放的位置
	 */
	String current = "current";
	
	/**
	 * 要播放的音乐的列表下标值
	 */
	String listPosition = "listPosition";
	
	/**
	 * 要播放的音乐列表的音乐类型
	 */
	String typeMusicFile = "type_music_file";
	
	/**
	 * 音乐播放控制信息
	 */
	String playerControlMsg = "MSG";
	
	/**
	 * 音乐的播放模式
	 */
	String control = "control";
	
	/**
	 * 要播放音乐的路径
	 */
	String url = "url";
	
	/**
	 * 当前的播放进度
	 */
	String currentProgress = "currentProgress";
	
	/**
	 * 当前的播放时长
	 */
	String currentTime = "currentTime";
}
