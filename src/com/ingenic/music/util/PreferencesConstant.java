/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.util;

public interface PreferencesConstant {
	
	/**
	 * 首选项的文件名
	 */
	String sharedPrefsFileName = "ingenic_music";
	
	/**
	 * 是否第一次进入本应用
	 */
	String isFirst = "isFirst";
	
	/**
	 * 当前播放的音乐位置
	 */
	String currentPosition = "current_position";
	
	/**
	 * 当前播放音乐的时间
	 */
	String currentTime ="currentTime";
	/**
	 * 当前播放列表音乐文件的类型
	 */
	String typeMusicFlie = "type_music_file";
	
	/**
	 * 切换音乐文件类型之前播放音乐文件的类
	/**型
	 */
	String oldTypeMusicFile = "mOldType";
	
	/**
	 * 当前播放音乐的路径
	 */
	String currentUrl = "current_url";
	
	/**
	 * 当前音乐文件是否在正在播放状态
	 */
	String isPlay = "isPlay";
	
	/**
	 * 记录退出应用时，正在进行的音乐是否非暂停状态,这个值由服务存储，如果为非暂停状态就要杀死后台播放服务
	 */
	String isPlaying = "isPlaying";
	
	/**
	 * 音乐列表的播放模式
	 */
	String mode = "mode";
	
	/**
	 * 后台音乐播放服务是否已经死了
	 */
	String is_service_die = "is_service_die";
	/**
	 * 操作是否完成
	 */
	String isOk = "isOk";
}
