/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.music.util;

import com.ingenic.iwds.HardwareList;

public class Utils {

	/**
	 * 判断当前的屏是园还是方形
	 *
	 * @return
	 */
	public static boolean IsCircularScreen() {
		return HardwareList.IsCircularScreen();
	}

	private static long lastClickTime;

	/**
	 * 防止按钮连续点击
	 *
	 * @return
	 */
	public synchronized static boolean isFastClick() {
		long time = System.currentTimeMillis();
		if (time - lastClickTime < 1000) {
			return true;
		}
		lastClickTime = time;
		return false;
	}

}
