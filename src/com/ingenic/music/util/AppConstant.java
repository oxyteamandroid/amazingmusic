/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.util;

/**
 * 应用常量类 本应用中的一些常量类。存放播放控制信息
 * 
 * @author MingDan
 * 
 */
public class AppConstant {
	public class PlayerMsg {
		public static final int PLAY_MSG = 1; // 播放
		public static final int PAUSE_MSG = 2; // 暂停
		public static final int STOP_MSG = 3; // 停止
		public static final int CONTINUE_MSG = 4; // 继续
		public static final int PRIVIOUS_MSG = 5; // 上一首
		public static final int NEXT_MSG = 6; // 下一首
		public static final int PROGRESS_CHANGE = 7;// 进度改变
		public static final int PLAYING_MSG = 8; // 正在播放
	}

	/**
	 * 播放音乐的服务
	 */
	public static final String MUSIC_SERVICE = "com.ingenic.music.action.MUSIC_SERVICE";

	/**
	 * 更新播放界面
	 */
	public static final String UPDATE_ACTION = "com.ingenic.music.action.UPDATE_ACTION";

	/**
	 * 更改播放模式
	 */
	public static final String CTL_ACTION = "com.ingenic.music.action.CTL_ACTION";

	/**
	 * 当前音乐改变动作（被动更新进度）
	 */
	public static final String MUSIC_CURRENT = "com.ingenic.music.action.MUSIC_CURRENT";

	/**
	 * 当前音乐改变动作（用户主动更新进度)
	 */
	public static final String PROGRESS_CHANGE = "com.ingenic.music.action.PROGRESS_CHANGE";
	/**
	 * 音乐后台播放服务die，通知界面刷新的action
	 */
	public static final String PLAYER_SERVICE_DIE = "com.ingenic.music.action.PLAYER_SERVICE_DIE";

	/**
	 * 闹钟响铃时的广播ACTION
	 */
	public static final String ALARM_BELL_STOP_MUSIC = "com.ingenic.clock.start";

	/**
	 * 闹钟停止响铃时的广播ACTION
	 */
	public static final String ALARM_STOP_MUSIC_START = "com.ingenic.clock.stop";

	/**
	 * 播放录音时的广播ACTION
	 */
	public static final String RECORDER_STOP_MUSIC = "com.ingenic.amazingrecorder.start";

	/**
	 * 停止播放录音时的广播ACTION
	 */
	public static final String RECORDER_STOP_MUSIC_START = "com.ingenic.amazingrecorder.stop";
	/**
	 * 更新本地音乐播放器的widget界面
	 */
	public static final String  CHANGE_WIDGET_UI= "com.ingenic.music.action.CHANGE_WIDGET_UI";
	/**
	 * 点击暂停播放按钮更新本地音乐播放器的Widget界面
	 */
	public static final String  PAUSE_RESUME_CHANGE_WIDGET_UI = "com.ingenic.music.action.PAUSE_RESUME_CHANGE_WIDGET_UI";
	/**
	 * 下一曲的action
	 */
	public static final String NEXT_MUSIC = "com.ingenic.music.action.NEXT_MUSIC";
	/**
	 * 上一曲的action
	 */
	public static final String PREVIOUS_MUSIC = "com.ingenic.music.action.PREVIOUS_MUSIC";
	/**
	 * 暂停和继续播放音乐的action
	 */
	public static final String PAUSE_RESUME = "com.ingenic.music.action.PAUSE_RESUME";
	/**
	 * 通话中心正在呼叫时的广播ACTION
	 */
	public static final String ACTION_OUT_CALL = "com.ingenic.mobilecenter.action.OUT_CALL";
	/**
	 * 通话中心来电时的广播ACTION
	 */
	public static final String ACTION_INCOMING_CALL = "com.ingenic.mobilecenter.action.INCOMING_CALL";
	/**
	 * 通话中心挂断后的广播ACTION
	 */
	public static final String ACTION_STOP_CALL = "com.ingenic.mobilecenter.action.STOP_CALL";
	/**
	 * service重新获取音乐集合action
	 */
	public static final String SERVICE_GET_MUSICLIST = "com.ingenic.music.action.SERVICE_GET_MUSICLIST";

}
