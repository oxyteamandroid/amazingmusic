/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MusicTypeSwitchAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<String> mList;

	public MusicTypeSwitchAdapter(Context context) {
		mContext = context;
	}

	public void setList(ArrayList<String> list) {
		if (mList != null)
			mList.clear();
		mList = list;
	}

	@Override
	public int getCount() {
		if (mList != null) {
			return mList.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (mList != null) {
			return mList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		if (mList != null) {
			return position;
		}
		return -1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView tv = new TextView(mContext);
		tv.setTextColor(Color.RED);
		tv.setTextSize(18);
		tv.setWidth(100);
		tv.setHeight(80);
		tv.setGravity(Gravity.CENTER);
		String str = mList.get(position);
		tv.setText(str);
		return tv;
	}

}
