/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 *
 * Elf/AmazingMusic
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingenic.music.R;
import com.ingenic.music.util.PreferencesConstant;
import com.ingenic.music.view.SwipeLayout;
import com.ingenic.music.view.SwipeLayout.OnSwipeListener;

/**
 * 2015/7/31
 *
 * 下拉刷新列表适配器
 *
 * @author MingDan
 *
 */
public class MusicListAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<HashMap<String, Object>> mList;

	/**
	 * 删除监听器
	 */
	private RemoveListener mRemoveListener;

	/**
	 * 使Activity右滑退出功能失效的监听器
	 */
	private DisableListener mDisableListener;
	private static final long ENABLE_DELAY_TIME = 100;

	public interface RemoveListener {
		public void removeItem(View v, int position);
	}

	public interface DisableListener {
		public void disableScrollView();

		public void enableScrollView();
	}

	public MusicListAdapter(Context context) {
		mContext = context;
	}

	public void setList(ArrayList<HashMap<String, Object>> list) {
		if (mList != null)
			mList.clear();
		mList = list;
	}

	/**
	 * 设置删除监听
	 * 
	 * @param removeListener
	 */
	public void setRemoveListener(RemoveListener removeListener) {
		this.mRemoveListener = removeListener;
	}

	/**
	 * 设置使Activity右滑退出功能失效的监听器
	 */
	public void setDisableListener(DisableListener disableListener) {
		this.mDisableListener = disableListener;
	}

	@Override
	public int getCount() {
		if (mList != null) {
			return mList.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (mList != null) {
			return mList.get(position);
		}
		return null;

	}

	@Override
	public long getItemId(int position) {
		if (mList != null) {
			return position;
		}
		return -1;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		SharedPreferences preferences = mContext.getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName, Context.MODE_PRIVATE);
		ViewHolder holder;
		final View view = LayoutInflater.from(mContext).inflate(R.layout.music_list_item, null);
		holder = new ViewHolder();
		holder.title = (TextView) view.findViewById(R.id.title);
		holder.artist = (TextView) view.findViewById(R.id.artist);
		holder.playing = (ImageView) view.findViewById(R.id.playing);
		holder.delete = (Button) view.findViewById(R.id.delete);
		holder.notification = (RelativeLayout) view.findViewById(R.id.notification);
		view.setTag(holder);
		/*
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.music_list_item, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.artist = (TextView) convertView.findViewById(R.id.artist);
			holder.playing = (ImageView) convertView.findViewById(R.id.playing);
			holder.delete = (Button) convertView.findViewById(R.id.delete);
			holder.notification = (RelativeLayout) convertView
					.findViewById(R.id.notification);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		*/

		if (mList != null && mList.get(position) != null) {
			holder.title.setText(mList.get(position).get("title").toString());

			if (preferences.getInt(PreferencesConstant.typeMusicFlie, 0) == 0) {
				holder.artist.setText(mList.get(position).get("artist")
						.toString());
			} else if (preferences.getInt(PreferencesConstant.typeMusicFlie, 0) == 1) {
				holder.artist.setText(mList.get(position).get("duration")
						.toString());
			}

			// 点击delete按钮，执行删除操作
			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// 调用删除监听中的删除方法
					if (mRemoveListener != null) {
						mRemoveListener.removeItem(view, position);
					}
				}
			});

			String currentUrl = preferences.getString(
					PreferencesConstant.currentUrl, "");
			String positionUrl = mList.get(position).get("url").toString();

			if (!currentUrl.equals("") && currentUrl.equals(positionUrl)) {
				preferences.edit()
						.putInt(PreferencesConstant.currentPosition, position)
						.commit();
				holder.playing.setVisibility(View.VISIBLE);
				holder.notification
						.setBackgroundResource(R.drawable.bg_music_item_pressed);
				if(preferences.getBoolean(PreferencesConstant.isPlaying, false)) {
					holder.playing.setImageResource(R.drawable.music_playing);
				} else {
					holder.playing.setImageResource(R.drawable.voice_pause);
				}
			} else {
				holder.playing.setVisibility(View.GONE);
				holder.notification
						.setBackgroundResource(R.drawable.bg_music_item_normal);
			}
		}

		//swipeLayout = (SwipeLayout) convertView;
		swipeLayout = (SwipeLayout) view;
		swipeLayout.setOnSwipeListener(osl);
		//return convertView;
		return view;
	}

	class ViewHolder {
		TextView title;
		TextView artist;
		ImageView playing;
		Button delete;
		RelativeLayout notification;
	}

	public List<SwipeLayout> openedSwipeLayouts = new ArrayList<SwipeLayout>();

	private OnSwipeListener osl = new OnSwipeListener() {

		@Override
		public void onOpen(final SwipeLayout swipeLayout, boolean open) {
			if (open) {
				openedSwipeLayouts.add(swipeLayout);
			} else {
				openedSwipeLayouts.remove(swipeLayout);
				new Thread() {
					public void run() {
						SystemClock.sleep(ENABLE_DELAY_TIME);
						swipeLayout.isEnabled = true;
						if (mDisableListener != null) {
							mDisableListener.enableScrollView(); // 恢复Activity右滑退出功能
						}
					};
				}.start();
			}
		}

		@Override
		public void onDraging(SwipeLayout swipeLayout, float percent) {
			if (openedSwipeLayouts.size() > 0) {

				/**
				 * 使ListView的条目点击事件失效
				 */
				swipeLayout.isEnabled = false;

				/**
				 * 使Activity右滑退出功能禁用
				 */
				if (mDisableListener != null) {
					mDisableListener.disableScrollView();
				}

				// 防止出现多次移除的bug
				for (int i = openedSwipeLayouts.size() - 1; i >= 0; i--) {
					openedSwipeLayouts.get(i).close();
					openedSwipeLayouts.remove(i);
				}
			} else {

			}

		}
	};
	public SwipeLayout swipeLayout;
}
