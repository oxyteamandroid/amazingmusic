/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.ui;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.music.R;
import com.ingenic.music.bean.MusicInfo;
import com.ingenic.music.service.PlayerService;
import com.ingenic.music.util.AppConstant;
import com.ingenic.music.util.IntentConstant;
import com.ingenic.music.util.MediaUtil;
import com.ingenic.music.util.PreferencesConstant;
import com.ingenic.music.util.Utils;
import com.ingenic.music.view.NoTouchSeekBar;
import com.ingenic.music.view.RingProgressView;
import com.ingenic.music.view.RotateImageView;

public class MusicPlayingActivity extends RightScrollActivity {
	private RightScrollView mView;

	/**
	 * 歌曲标题
	 */
	private TextView mTitle;

	/**
	 * 下一首歌曲的标题
	 */
	private TextView mNextTitle;

	/**
	 * 歌曲的演唱者
	 */
	private TextView mArtist;

	/**
	 * 播放进度条
	 */
	private RingProgressView mMusicProgress;

	/**
	 * 暂停播放图片按钮
	 */
	private ImageView mPlayBtn;

	/**
	 * 音量设置SeekBar
	 */
	private NoTouchSeekBar mVolumeSeek;

	/**
	 * 专辑图片
	 */
	private RotateImageView mAlbum;

	/**
	 * 回退按钮
	 */
	private ImageButton mBack;

	/**
	 * 播放模式
	 */
	private ImageButton mPlayMode;

	/**
	 * 减小音量按钮布局
	 */
	private LinearLayout mDecrease;

	/**
	 * 增加音量按钮布局
	 */
	private LinearLayout mAdd;

	/**
	 * 减小音量按钮
	 */
	private ImageButton mDecreaseView;

	/**
	 * 增大音量按钮
	 */
	private ImageButton mAddView;

	/**
	 * 上一曲按鈕
	 */
	private Button mPreviousMusic;

	/**
	 * 下一曲按鈕
	 */
	private Button mNextMusic;

	/**
	 * 播放歌曲的位置
	 */
	private int listPosition;

	/**
	 * 当前歌曲播放时间
	 */
	private int currentTime;

	/**
	 * 播放模式的标识
	 */
	private int mode;

	/**
	 * 录音文件的后缀名字符串常量
	 */
	private static final String RECORDER_FILE_EXTENSION = "amr";

	/**
	 * 播放进度控制比率常量
	 */
	private static final int PROGRESS_RATIO = 5;

	/**
	 * 音量控制比率常量
	 */
	private static final int VOLUMN_RATIO = 5;

	/**
	 * 专辑图片的旋转的属性动画
	 */
	// private ObjectAnimator mAnim;

	/**
	 * 记录选中的播放的音乐文件的类型
	 */
	private int type_music_file = LOCAL_MUSIC_FILE;
	private static final int LOCAL_MUSIC_FILE = 0; // mp3音乐文件
	private static final int RECORDER_MUSIC_FILE = 1; // 录音文件

	private ArrayList<MusicInfo> mMusicInfos;

	private PlayerReceiver playerReceiver;

	private boolean isPlay = false;

	private int msg = -1;

	private AudioManager mAudioManager;
	private int maxVolume;
	private int currentVolume;

	private SharedPreferences preferences;

	/**
	 * 当前播放的音乐信息
	 */
	private MusicInfo mMusic;

	/**
	 * 定义三个播放模式的常量
	 */
	private static final int MODE_REPEAT_ONE = 1;
	private static final int MODE_REPEAT_ALL = 2;
	private static final int MODE_SHUFFLE = 3;

	/**
	 * 定义三个Handler处理消息的what
	 */
	private static final int REFRESH_UI = 1;
	private static final int NEXT_MUSIC = 2;
	private static final int PREVIOUS_MUSIC = 3;

	/**
	 * getScaledTouchSlop是一个距离，表示滑动的时候，手的移动要大于这个距离才开始移动控件。
	 * 如果小于这个距离就不触发移动控件，如ViewPager就是用这个距离来判断用户是否翻页
	 */
	// private int mTouchSlop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View contentView = null;

		if (Utils.IsCircularScreen()) {
			contentView = getLayoutInflater().inflate(
					R.layout.naturalround_music_playing_layout, null);
		} else {
			contentView = getLayoutInflater().inflate(
					R.layout.wisesquare_music_playing_layout, null);
		}

		if (mView == null) {
			mView = getRightScrollView();
		}

		mView.setContentView(contentView);

		// final ViewConfiguration configuration = ViewConfiguration.get(this);
		// mTouchSlop = configuration.getScaledTouchSlop();

		preferences = getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName, Context.MODE_PRIVATE);

		/**
		 * 获取播放信息
		 */
		getDataFromBundle();

		/**
		 * 初始化播放界面
		 */
		init();

		/**
		 * 初始化专辑图片的旋转动画
		 */
		// initRotationAnimator();

		/**
		 * 获取分类的播放列表
		 */
		getmMusicInfos();

		/**
		 * 根据listPosition获取当前播放的音乐信息
		 */
		getCurrentMusicInfo();

		/**
		 * 播放模式的切换
		 */
		initPlayMode();

		if (msg == AppConstant.PlayerMsg.PLAY_MSG) {
			// 直接播放歌曲
			play();
		} else if (msg == AppConstant.PlayerMsg.PLAYING_MSG) {
			// 继续播放歌曲
			continuePlay();
		}

		/**
		 * 注册控制音乐播放的广播接收器
		 */
		registerReceiver();

		/**
		 * 设置播放进度
		 */
		setPlayerProgress();

		/**
		 * 设置音量
		 */
		setPlayerVolume();

		/**
		 * 设置点击事件进行歌曲切换，上一曲，下一曲
		 */
		setPreviousAndNextMusicOnClickListener();

		/**
		 * 设置长按上一曲下一曲按钮，播放进度快进快退
		 */
		setOnLongClickListener();

		/**
		 * 设置点击按钮，控制音量的增加和减小
		 */
		setAddAndDecreaseVolumnListener();

		/**
		 * 给专辑图片设置点击事件
		 */
		setPauseAndContinueListener();

		/**
		 * 给回退按钮设置点击退出功能
		 */
		setBackListener();

	}

	/**
	 * 根据listPosition获取当前播放的音乐信息
	 */
	private void getCurrentMusicInfo() {
		if (listPosition >= 0) {
			if(listPosition >= mMusicInfos.size()) {
				listPosition = mMusicInfos.size()-1;
			}
			mMusic = mMusicInfos.get(listPosition);
			mTitle.setText(mMusic.title);
			if (type_music_file == LOCAL_MUSIC_FILE) {
				mArtist.setText(mMusic.artist);
			}
		}
	}

	/**
	 * 给回退按钮设置点击退出功能
	 */
	private void setBackListener() {
		mBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 退出播放界面
				onBackPressed();
				overridePendingTransition(0, R.anim.slide_out_to_right);
			}
		});
	}

	/**
	 * 给专辑图片设置点击事件
	 */
	private void setPauseAndContinueListener() {
		mAlbum.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				if (isPlay) {
					// 暂停播放
					mTitle.setEllipsize(TruncateAt.END);
					mNextTitle.setEllipsize(TruncateAt.END);
					mPlayBtn.setImageResource(R.drawable.play_to_pause_anim_list);
					((AnimationDrawable) mPlayBtn.getDrawable()).start();
					pause();
				} else {
					// 继续播放
					mTitle.setEllipsize(TruncateAt.MARQUEE);
					mNextTitle.setEllipsize(TruncateAt.MARQUEE);
					mPlayBtn.setImageResource(R.drawable.pause_to_play_anim_list);
					((AnimationDrawable) mPlayBtn.getDrawable()).start();

					/*
					 * if (mAnim.isPaused()) { mAnim.resume(); }
					 */

					mAlbum.resume();

					isPauseToContinue = true;
					continuePlay();
				}

			}
		});
	}

	/**
	 * 点击按钮，控制音量的增加和减小
	 */
	private void setAddAndDecreaseVolumnListener() {
		mDecrease.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentVolume = currentVolume - maxVolume / VOLUMN_RATIO;

				if (currentVolume < 0) {
					currentVolume = 0;
					//return;
				}

				mVolumeSeek.setProgress(currentVolume);

				// 设置音量
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						currentVolume, 0);
			}
		});

		/**
		 * 设置点击效果
		 */
		mDecrease.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					mDecreaseView.setImageResource(R.drawable.decrease_pressed);
					break;

				case MotionEvent.ACTION_UP:
					mDecreaseView.setImageResource(R.drawable.decrease_normal);
					break;

				default:
					break;
				}

				return false;
			}
		});

		mAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentVolume = currentVolume + maxVolume / VOLUMN_RATIO;
				if (currentVolume > maxVolume) {
					currentVolume = maxVolume;
					//return;
				}

				mVolumeSeek.setProgress(currentVolume);
				// 设置音量
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						currentVolume, 0);
			}
		});

		/**
		 * 设置点击效果
		 */
		mAdd.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					mAddView.setImageResource(R.drawable.decrease_pressed);
					break;

				case MotionEvent.ACTION_UP:
					mAddView.setImageResource(R.drawable.decrease_normal);
					break;

				default:
					break;
				}

				return false;
			}
		});
	}

	/**
	 * 长按事件，歌曲播放快进，快退
	 */
	private void setOnLongClickListener() {
		mPreviousMusic.setOnTouchListener(new OnTouchListener() {
			private int startX = -1;
			private int startY = -1;
			private long startTime = 0;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:

					mPreviousMusic.setBackgroundResource(R.drawable.previous_pressed);

					startX = (int) event.getX();
					startY = (int) event.getY();
					startTime = System.currentTimeMillis();
					break;
				case MotionEvent.ACTION_MOVE:
					// 防止点击过快，在按下的时候没有获取到点的位置
					if (startX == -1 || startY == -1) {
						startX = (int) event.getX();
						startY = (int) event.getY();
					}
					break;
				case MotionEvent.ACTION_UP:

					mPreviousMusic
							.setBackgroundResource(R.drawable.previous_normal);

					int endX = (int) event.getX();
					int endY = (int) event.getY();
					long endTime = System.currentTimeMillis();
					int disX = Math.abs(endX - startX);
					int disY = Math.abs(endY - startY);
					long disTime = endTime - startTime;
					if (disX < 20 && disY < 20) {
						// 点击事件
						if (disTime > 500) { // 长按事件
							int disProgress = (int) ((PROGRESS_RATIO * disTime / 1000));
							mCurrentProgress = mCurrentProgress - disProgress;
							currentTime = (int) ((mMusic.duration / 100) * mCurrentProgress);
							audioTrackChange(currentTime);
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}
				default:
					break;
				}
				return false;
			}
		});

		mNextMusic.setOnTouchListener(new OnTouchListener() {
			private int startX = -1;
			private int startY = -1;
			private long startTime = 0;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					mNextMusic.setBackgroundResource(R.drawable.next_pressed);

					startX = (int) event.getX();
					startY = (int) event.getY();
					startTime = System.currentTimeMillis();
					break;
				case MotionEvent.ACTION_MOVE:
					//mNextMusic.setBackgroundResource(R.drawable.next_normal);
					// 防止点击过快，在按下的时候没有获取到点的位置
					if (startX == -1 || startY == -1) {
						startX = (int) event.getX();
						startY = (int) event.getY();
					}
					int moveX = (int) event.getX();
					int moveY = (int) event.getY();
					long endTime_move = System.currentTimeMillis();
					int disX_move = Math.abs(moveX - startX);
					int disY_move = Math.abs(moveY - startY);
					long disTime_move = endTime_move - startTime;
					if (disX_move < 20 && disY_move < 20) {
						// 点击事件
						int disProgress = (int) ((PROGRESS_RATIO * disTime_move / 1000));
						mCurrentProgress = mCurrentProgress + disProgress;
						currentTime = (int) ((mMusic.duration / 100) * mCurrentProgress);
						audioTrackChange(currentTime);

					} else {
						return false;
					}
					startX = moveX;
					startY = moveY;
					//startTime = endTime_move;
					break;
				case MotionEvent.ACTION_UP:
					mNextMusic.setBackgroundResource(R.drawable.next_normal);

					int endX = (int) event.getX();
					int endY = (int) event.getY();
					long endTime = System.currentTimeMillis();
					int disX = Math.abs(endX - startX);
					int disY = Math.abs(endY - startY);
					long disTime = endTime - startTime;
					if (disX < 20 && disY < 20) {
						// 点击事件
						if (disTime > 500) { // 长按事件，拦截掉自己处理
							return true;
						} else {
							return false;
						}

					} else {
						return false;
					}
				default:
					break;
				}
				return false;
			}
		});
	}

	/**
	 * 设置点击事件进行歌曲切换，上一曲，下一曲
	 */
	private void setPreviousAndNextMusicOnClickListener() {
		mPreviousMusic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				/**
				 * 过滤掉连续的无效点击
				 */
				if (!Utils.isFastClick()) {

					/**
					 * 在发送播放上一曲消息指令之前清空之前所有的播放上一曲指令的消息
					 */
					mHandler.removeMessages(PREVIOUS_MUSIC);
					mHandler.sendEmptyMessage(PREVIOUS_MUSIC);
				}

			}
		});

		mNextMusic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				/**
				 * 过滤掉连续的无效点击
				 */
				if (!Utils.isFastClick()) {
					/**
					 * 在发送播放下一曲消息指令之前清空之前所有的播放下一曲指令的消息
					 */
					mHandler.removeMessages(NEXT_MUSIC);
					mHandler.sendEmptyMessage(NEXT_MUSIC);
				}

			}
		});
	}

	/**
	 * 获取分类的播放列表
	 */
	private void getmMusicInfos() {
		mMusicInfos = MediaUtil.getMusicInfos(getApplicationContext());

		ArrayList<MusicInfo> mMp3Infos = new ArrayList<MusicInfo>();
		ArrayList<MusicInfo> mRecorderInfos = new ArrayList<MusicInfo>();
		for (MusicInfo info : mMusicInfos) {
			String fileExtension = MediaUtil.getFileExtension(new File(
					info.displayName));
			if (fileExtension.equals(RECORDER_FILE_EXTENSION)) {
				mRecorderInfos.add(info);
			} else {
				mMp3Infos.add(info);
			}
		}

		if (type_music_file == LOCAL_MUSIC_FILE) {
			mMusicInfos = mMp3Infos;
		} else if (type_music_file == RECORDER_MUSIC_FILE) {
			mMusicInfos = mRecorderInfos;
		}
	}

	/**
	 * 初始化专辑图片的旋转动画
	 */
	/*
	 * private void initRotationAnimator() { mAnim =
	 * ObjectAnimator.ofFloat(mAlbum, "rotation", 0, 360);
	 * mAnim.setDuration(1000 * 30); mAnim.setRepeatCount(20);
	 * mAnim.setRepeatMode(ObjectAnimator.RESTART); }
	 */

	/**
	 * 初始化播放模式
	 */
	private void initPlayMode() {

		mode = preferences.getInt(PreferencesConstant.mode, MODE_REPEAT_ALL); // 默认为列表循环播放模式

		if (mPlayMode != null) {
			switch (mode) {
			case MODE_REPEAT_ONE:
				mPlayMode.setImageResource(R.drawable.repeat_one_selector);
				repeat_one();
				break;
			case MODE_REPEAT_ALL:
				mPlayMode.setImageResource(R.drawable.repeat_all_selector);
				repeat_all();
				break;
			case MODE_SHUFFLE:
				mPlayMode.setImageResource(R.drawable.shuffle_selector);
				shuffleMusic();
				break;
			default:
				break;
			}

			mPlayMode.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					// 防止过快的点击 Toast显示不过来
					if (!MediaUtil.isFastDoubleClick()) {
						switch (mode) {
						case MODE_REPEAT_ONE: // 当前为单曲循环播放模式 点击设置为随机播放模式
							mode = MODE_SHUFFLE;
							mPlayMode
									.setImageResource(R.drawable.shuffle_selector);
							shuffleMusic();
							break;
						case MODE_REPEAT_ALL: // 当前为全部循环播放模式 点击设置为单曲循环播放模式
							mode = MODE_REPEAT_ONE;
							mPlayMode
									.setImageResource(R.drawable.repeat_one_selector);
							repeat_one();
							break;
						case MODE_SHUFFLE: // 当前为随机播放模式 点击设置为全部循环播放模式
							mode = MODE_REPEAT_ALL;
							mPlayMode
									.setImageResource(R.drawable.repeat_all_selector);
							repeat_all();
							break;
						default:
							break;
						}
						// 保存当前的播放模式
						preferences.edit()
								.putInt(PreferencesConstant.mode, mode)
								.commit();
					}
				}
			});
		}
	}

	/**
	 * 单曲循环
	 */
	public void repeat_one() {
		Intent intent = new Intent(AppConstant.CTL_ACTION);
		intent.putExtra(IntentConstant.control, MODE_REPEAT_ONE);
		sendBroadcast(intent);
	}

	/**
	 * 全部循环
	 */
	public void repeat_all() {
		Intent intent = new Intent(AppConstant.CTL_ACTION);
		intent.putExtra(IntentConstant.control, MODE_REPEAT_ALL);
		sendBroadcast(intent);
	}

	/**
	 * 随机播放
	 */
	public void shuffleMusic() {
		Intent intent = new Intent(AppConstant.CTL_ACTION);
		intent.putExtra(IntentConstant.control, MODE_SHUFFLE);
		sendBroadcast(intent);
	}

	/**
	 * 定义和注册广播接收器
	 */
	private void registerReceiver() {
		playerReceiver = new PlayerReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(AppConstant.MUSIC_CURRENT);
		filter.addAction(AppConstant.UPDATE_ACTION);
		registerReceiver(playerReceiver, filter);
	}

	/**
	 * 从Bundle中获取数据
	 */
	private void getDataFromBundle() {
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		listPosition = bundle.getInt(IntentConstant.listPosition);
		type_music_file = bundle.getInt(IntentConstant.typeMusicFile);
		msg = bundle.getInt(IntentConstant.playerControlMsg);
	}

	/**
	 * 初始化控件
	 */
	private void init() {
		mMusicProgress = (RingProgressView) findViewById(R.id.playing_progress);
		mTitle = (TextView) findViewById(R.id.playing_title);
		mArtist = (TextView) findViewById(R.id.tv_artist);
		mNextTitle = (TextView) findViewById(R.id.playing_title_next);
		mAlbum = (RotateImageView) findViewById(R.id.iv_album);
		mBack = (ImageButton) findViewById(R.id.ib_back);
		mPlayBtn = (ImageView) findViewById(R.id.playBtn);
		mVolumeSeek = (NoTouchSeekBar) findViewById(R.id.volume_seekbar);
		mPreviousMusic = (Button) findViewById(R.id.previous_music);
		mNextMusic = (Button) findViewById(R.id.next_music);
		mDecrease = (LinearLayout) findViewById(R.id.ll_decrease);
		mAdd = (LinearLayout) findViewById(R.id.ll_add);
		mDecreaseView = (ImageButton) findViewById(R.id.ib_decrease);
		mAddView = (ImageButton) findViewById(R.id.ib_add);
		mPlayMode = (ImageButton) findViewById(R.id.play_mode);

		// 设置标题滚动
		mTitle.setEllipsize(TruncateAt.MARQUEE);
		mNextTitle.setEllipsize(TruncateAt.MARQUEE);
	}

	/**
	 * 设置播放器播放进度
	 */
	private void setPlayerProgress() {

		arrayIndexOutOfBoundsExceptionHandling(this);

		if (listPosition < 0) {
			MusicPlayingActivity.this.finish();
		}
	}

	/**
	 * 设置播放器音量
	 */
	private void setPlayerVolume() {
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC); // 获取系统最大音量
		currentVolume = mAudioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC); // 获取当前值
		mVolumeSeek.setMax(maxVolume); // 拖动条最高值与系统最大声匹配
		mVolumeSeek.setProgress(currentVolume);
	}

	/**
	 * 播放音乐
	 */
	public void play() {
		isPlay = true;

		arrayIndexOutOfBoundsExceptionHandling(this);

		if (listPosition >= 0) {

			// 开异步任务去获取专辑图片并设置给mAlbum控件
			new ImageLoader().execute("load");

			/*
			 * if (mAnim.isPaused()) { mAnim.resume(); } else { mAnim.start(); }
			 */
			mAlbum.resume();

			Intent intent = new Intent(this, PlayerService.class);
			intent.setAction(AppConstant.MUSIC_SERVICE);
			intent.putExtra(IntentConstant.listPosition, listPosition);
			intent.putExtra(IntentConstant.url, mMusic.url);
			intent.putExtra(IntentConstant.playerControlMsg,
					AppConstant.PlayerMsg.PLAY_MSG);
			intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
			startService(intent);
		} else {
			this.finish();
		}
	}

	/**
	 * 暂停播放
	 */
	public void pause() {
		isPlay = false;

		// mAnim.pause();
		mAlbum.pause();

		Intent intent = new Intent(this, PlayerService.class);
		intent.setAction(AppConstant.MUSIC_SERVICE);
		intent.putExtra(IntentConstant.playerControlMsg,
				AppConstant.PlayerMsg.PAUSE_MSG);
		intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
		startService(intent);
	}

	/**
	 * 继续播放
	 */
	public void continuePlay() {
		isPlay = true;

		arrayIndexOutOfBoundsExceptionHandling(this);

		if (listPosition >= 0) {
			if (!isPauseToContinue) {

				// 开异步任务去获取专辑图片并设置给mAlbum控件
				new ImageLoader().execute("load");

				/*
				 * if (mAnim.isPaused()) { mAnim.resume(); } else {
				 * mAnim.start(); }
				 */
				mAlbum.resume();
			}

			Intent intent = new Intent(this, PlayerService.class);
			intent.setAction(AppConstant.MUSIC_SERVICE);
			intent.putExtra(IntentConstant.listPosition, listPosition);
			intent.putExtra(IntentConstant.url, mMusic.url);
			intent.putExtra(IntentConstant.playerControlMsg,
					AppConstant.PlayerMsg.CONTINUE_MSG);
			intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
			startService(intent);
		} else {
			this.finish();
		}
	}

	/**
	 * 播放上一首歌曲
	 */
	public void previous() {

		if (preferences.getBoolean(PreferencesConstant.isOk, false)) {
			preferences.edit().putBoolean(PreferencesConstant.isOk, false).commit();

			if (mAlbum != null) {
				mAlbum.pause();
			}

			// new Thread(new ProgressRunable(mCurrentProgress)).start();

			mPlayBtn.setImageResource(R.drawable.pause_to_play_anim_list);
			((AnimationDrawable) mPlayBtn.getDrawable()).start();

			isPlay = true;

			if (mode == MODE_SHUFFLE) {
				listPosition = getRandomIndex(mMusicInfos.size() - 1);
			} else {
				listPosition = (listPosition - 1 + mMusicInfos.size())
						% mMusicInfos.size();
			}
			mMusic = mMusicInfos.get(listPosition);
			mNextTitle.setText(mTitle.getText());
			mTitle.setText(mMusic.title);
			if (type_music_file == LOCAL_MUSIC_FILE) {
				mArtist.setText(mMusic.artist);
			}

			mAlbum.setImageResource(R.drawable.defaultalbum); // 在获取到专辑图片之前使用默认的

			Intent intent = new Intent(this, PlayerService.class);
			intent.setAction(AppConstant.MUSIC_SERVICE);
			intent.putExtra(IntentConstant.listPosition, listPosition);
			intent.putExtra(IntentConstant.url, mMusic.url);
			intent.putExtra(IntentConstant.playerControlMsg,
					AppConstant.PlayerMsg.PRIVIOUS_MSG);
			intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
			startService(intent);

			// 开异步任务去获取专辑图片并设置给mAlbum控件
			//new ImageLoader().execute("load");

			// mAnim.start();
			mAlbum.resume();
		}

	}

	/**
	 * 播放下一首歌曲
	 */
	public void next() {

		if (preferences.getBoolean(PreferencesConstant.isOk, false)) {

			preferences.edit().putBoolean(PreferencesConstant.isOk, false).commit();

			if (mAlbum != null) {
				mAlbum.pause();
			}

			// new Thread(new ProgressRunable(mCurrentProgress)).start();

			mPlayBtn.setImageResource(R.drawable.pause_to_play_anim_list);
			((AnimationDrawable) mPlayBtn.getDrawable()).start();

			isPlay = true;
			if (mode == MODE_SHUFFLE) {
				listPosition = getRandomIndex(mMusicInfos.size() - 1);
			} else {
				listPosition = (listPosition + 1) % mMusicInfos.size();
			}

			mMusic = mMusicInfos.get(listPosition);
			mNextTitle.setText(mTitle.getText());
			mTitle.setText(mMusic.title);
			if (type_music_file == LOCAL_MUSIC_FILE) {
				mArtist.setText(mMusic.artist);
			}

			mAlbum.setImageResource(R.drawable.defaultalbum); // 在获取到专辑图片之前使用默认的

			Intent intent = new Intent(this, PlayerService.class);
			intent.setAction(AppConstant.MUSIC_SERVICE);
			intent.putExtra(IntentConstant.listPosition, listPosition);
			intent.putExtra(IntentConstant.url, mMusic.url);
			intent.putExtra(IntentConstant.playerControlMsg,
					AppConstant.PlayerMsg.NEXT_MSG);
			intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
			startService(intent);

			// 开异步任务去获取专辑图片并设置给mAlbum控件
			//new ImageLoader().execute("load");

			// mAnim.start();
			mAlbum.resume();
		}

	}

	/**
	 * 获取随机位置
	 *
	 * @param end
	 * @return
	 */
	protected int getRandomIndex(int end) {
		int index = (int) (Math.random() * end);

		return index;
	}

	/**
	 * 播放进度改变
	 * 
	 * @param progress
	 */
	public void audioTrackChange(int progress) {
		Intent intent = new Intent(AppConstant.PROGRESS_CHANGE);
		intent.putExtra(IntentConstant.url, mMusicInfos.get(listPosition).url);
		intent.putExtra(IntentConstant.listPosition, listPosition);
		intent.putExtra(IntentConstant.currentProgress, progress);
		sendBroadcast(intent);
		isPlay = true;
		mPlayBtn.setImageResource(R.drawable.pause_to_play_anim_list);
		((AnimationDrawable) mPlayBtn.getDrawable()).start();
	}

	private int mCurrentProgress;

	/**
	 * 用来接收从service传回来的广播的内部类
	 * 
	 * @author MingDan
	 */
	public class PlayerReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(AppConstant.MUSIC_CURRENT)) {
				currentTime = intent
						.getIntExtra(IntentConstant.currentTime, -1);
				if (listPosition >= 0) {
					int duration = (int) mMusicInfos.get(listPosition).duration;

					if (duration > 0) {
						// 更新圆形进度
						mCurrentProgress = (int) (currentTime * 100 / duration);
						if (mCurrentProgress > 100) {
							mCurrentProgress = 100;
						}
						mMusicProgress.setProgress(mCurrentProgress);
					} else {
						AmazingToast.showToast(MusicPlayingActivity.this,
								R.string.failed_play,
								AmazingToast.LENGTH_SHORT,
								AmazingToast.BOTTOM_CENTER);
						MusicPlayingActivity.this.finish();
					}
				} else {
					MusicPlayingActivity.this.finish();
				}
			} else if (action.equals(AppConstant.UPDATE_ACTION)) {
				listPosition = intent.getIntExtra(IntentConstant.current, -1);

				if (listPosition != -1) {
					arrayIndexOutOfBoundsExceptionHandling(context);

					if (listPosition >= 0) {
						if (mMusicInfos.get(listPosition) != null) {
							mHandler.sendEmptyMessageDelayed(REFRESH_UI, 500);
						}
					} else {
						MusicPlayingActivity.this.finish();
					}
				} else {
					isPlay = false;
					mMusicProgress.setProgress(0);
					mTitle.setEllipsize(TruncateAt.END);
					mNextTitle.setEllipsize(TruncateAt.END);
					mPlayBtn.setImageResource(R.drawable.pause_to_play_anim_list);
					((AnimationDrawable) mPlayBtn.getDrawable()).start();
					listPosition = preferences.getInt(
							PreferencesConstant.currentPosition, -1);
				}
			}
		}
	}

	private void arrayIndexOutOfBoundsExceptionHandling(Context context) {
		if (listPosition >= mMusicInfos.size()) {
			listPosition = mMusicInfos.size() - 1;
		}
	}

	private boolean isPauseToContinue = false;

	@Override
	protected void onDestroy() {
		// 保存当前的是否正在播放音乐
		preferences.edit().putBoolean(PreferencesConstant.isPlay, isPlay)
				.commit();

		if (playerReceiver != null) {
			unregisterReceiver(playerReceiver);
		}

		/*
		 * if (mMusicInfos != null) { mMusicInfos.clear(); mMusicInfos = null; }
		 */

		super.onDestroy();
	}

	@Override
	protected void onPause() {

		/**
		 * 停止专辑图片的旋转
		 */
		mAlbum.pause();

		super.onPause();
	}

	@Override
	protected void onResume() {

		if (isPlay) {
			mAlbum.resume();
		}

		super.onResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
		overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
	}

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			/**
			 * 播放完这一曲后，自然播放下一曲时的除了服务要做些事，MusicPlayingActivity界面也要自己更新界面
			 */
			if (msg.what == REFRESH_UI) {
				if (mMusicInfos != null) {
					mMusic = mMusicInfos.get(listPosition);
					mTitle.setText(mMusic.title);
					if (type_music_file == LOCAL_MUSIC_FILE) {
						mArtist.setText(mMusic.artist);
					}

					mAlbum.setImageResource(R.drawable.defaultalbum); // 在未获取到专辑图片前使用默认的专辑图片

					// 开异步任务去获取专辑图片并设置给mAlbum控件
					new ImageLoader().execute("load");
				}
			} else if (msg.what == NEXT_MUSIC) {

				// 播放下一曲
				next();

			} else if (msg.what == PREVIOUS_MUSIC) {

				// 播放上一首
				previous();

			}
		};
	};

	/**
	 * 改变进度条进度的线程
	 */
	class ProgressRunable implements Runnable {

		private int currProgress;

		public ProgressRunable(int currPro) {
			currProgress = currPro;
		}

		@Override
		public void run() {
			// 不超出最大进度
			while (currProgress < 100) {

				// 步进值为1
				currProgress++;

				// 设置进度
				mMusicProgress.setProgress(currProgress);

				// 休眠10ms
				try {
					Thread.sleep(10);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 加载专辑图片的异步任务
	 * 
	 * @author MingDan
	 * 
	 */
	class ImageLoader extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			// 专辑图片的获取
			Bitmap bitmap = MediaUtil.getArtwork(MusicPlayingActivity.this,
					mMusic.id, mMusic.albumId, true, false);// 获取专辑位图对象，为大图
			bitmap = MediaUtil.toRoundBitmap(bitmap);
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			mAlbum.setImageBitmap(result);
		}
	}

}
