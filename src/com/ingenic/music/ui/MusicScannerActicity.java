/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.ui;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.music.R;
import com.ingenic.music.util.PreferencesConstant;

/**
 * 第一次进入应用时扫描本地音乐文件的MusicScannerActivity
 * 
 * 2015/7/30
 * 
 * @author MingDan
 * 
 */
public class MusicScannerActicity extends Activity {
	
	/**
	 * 扫描过程中的执行旋转动画的图片
	 */
	private ImageView mScanImgView;
	
	/**
	 * 扫描状态信息
	 */
	private TextView mTVScanState; 
	
	/**
	 * 旋转动画执行的时间常量
	 */
	private static final int ROTATE_ANIMATION_DURATION_TIME = 800;

	private static final int WAKELOCK_DURATION_TIME = 10000;
	
	private MediaScannerConnection mediaScanConn = null;
	private MusicSannerClient mClient = null;
	private File mFilePath = null;
	private String mFileType = null;

	private int fileCount = 1;
	
	private SharedPreferences preferences;
	
	private PowerManager powerManager = null;
	private WakeLock wakeLock = null;
	
	private static final int MUSIC_SCAN_COMPLETED = 1000;
	
	@SuppressLint("HandlerLeak")
	private Handler mScanCompletedHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == MUSIC_SCAN_COMPLETED) {
				mScanImgView.clearAnimation();
				mScanImgView.setVisibility(View.GONE);
				mTVScanState.setText(getString(R.string.scann_finished));
				
				//扫描结束要向首选项中已经进入本应用过的状态
				preferences.edit().putBoolean(PreferencesConstant.isFirst, false).commit();

				// 扫描结束进入播放列表界面
				Intent intent = new Intent(MusicScannerActicity.this,MusicListActivity.class);
				startActivity(intent);
				MusicScannerActicity.this.finish();
			}
		};
	};
	
	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.ui_music_scanner);
		
		preferences = getSharedPreferences(PreferencesConstant.sharedPrefsFileName,Context.MODE_PRIVATE);
		powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
		wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,"My Lock");
		
		if(!preferences.getBoolean(PreferencesConstant.isFirst, true)) {
			//如果已经不是第一次进入本应用就直接进入音乐列表界面，并退出
			Intent intent = new Intent(MusicScannerActicity.this,MusicListActivity.class);
			startActivity(intent);
			MusicScannerActicity.this.finish();
			return;
		}
		

		// 初始化MusicSannerClient
		mClient = new MusicSannerClient();
		mediaScanConn = new MediaScannerConnection(this, mClient);

		mScanImgView = (ImageView) this.findViewById(R.id.iv_scan);
		mTVScanState = (TextView) this.findViewById(R.id.tv_scan_state);

		// 使用代码定义动画，旋转动画
		RotateAnimation ra = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, 
				Animation.RELATIVE_TO_SELF, 0.5f);
		ra.setDuration(ROTATE_ANIMATION_DURATION_TIME);
		ra.setRepeatCount(Animation.INFINITE);
		ra.setInterpolator(new LinearInterpolator());// 匀速旋转
		mScanImgView.startAnimation(ra);
		
		// 开启异步任务扫描本地媒体文件，更新媒体库
		mSannerTask.execute("scanner");
		
	}
	
	private AsyncTask<String, Void, Void> mSannerTask = new AsyncTask<String, Void, Void>() {
		@Override
		protected Void doInBackground(String... arg0) {
			// 扫描媒体库
			scanfile(Environment.getExternalStorageDirectory().getAbsoluteFile());

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

	};

	/**
	 * 扫描本地媒体库
	 * 
	 * @author MingDan
	 */
	class MusicSannerClient implements MediaScannerConnection.MediaScannerConnectionClient {

		private int mScanFileCount;

		@Override
		public void onMediaScannerConnected() {
			if (mFilePath == null) {
				return;
			}

			mScanFileCount = 0;

			scan(mFilePath, mFileType);
		}

		@Override
		public void onScanCompleted(String path, Uri uri) {
			mScanFileCount++;

			if (fileCount == mScanFileCount) {
				// 扫描结束,断开连接
				mediaScanConn.disconnect();

				// 更新UI
				mScanCompletedHandler.sendEmptyMessage(MUSIC_SCAN_COMPLETED);
			}
		}

		private void scan(File file, String type) {
			// if (file.isFile()) {
			mediaScanConn.scanFile(file.getAbsolutePath(), null);
			// return;
			// }
			File[] files = file.listFiles();
			if (files == null) {
				return;
			}
			for (File f : file.listFiles()) {
				scan(f, type);
			}
		}
	}

	private void scanfile(File f) {
		this.mFilePath = f;
		getFileCount(f);
		mediaScanConn.connect();
	}

	/**
	 * 用于计数文件夹中所有文件和文件夹数量
	 * 
	 * @param f
	 */
	private void getFileCount(File f) {
		File[] fa = f.listFiles();
		for (int i = 0; i < fa.length; i++) {
			fileCount++;
			if (fa[i].isDirectory()) {
				getFileCount(fa[i]);
			}
		}
	}

	@Override
	protected void onDestroy() {
		
		super.onDestroy();
		
		if (mediaScanConn != null) {
			mediaScanConn.disconnect();
		}
	
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		wakeLock.acquire();
	}

	@Override
	protected void onPause() {
		super.onPause();
		new Thread() {
			public void run() {
				SystemClock.sleep(WAKELOCK_DURATION_TIME);
				wakeLock.release();
			};
		}.start();
	}
}
