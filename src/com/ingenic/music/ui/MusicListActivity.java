/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * MingTingZhu(MingDan)<tingzhu.ming@ingenic.com>
 * 
 * Elf/AmazingMusic
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.music.ui;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.music.R;
import com.ingenic.music.adapter.MusicListAdapter;
import com.ingenic.music.adapter.MusicListAdapter.DisableListener;
import com.ingenic.music.adapter.MusicListAdapter.RemoveListener;
import com.ingenic.music.adapter.MusicTypeSwitchAdapter;
import com.ingenic.music.bean.MusicInfo;
import com.ingenic.music.service.PlayerService;
import com.ingenic.music.util.AppConstant;
import com.ingenic.music.util.IntentConstant;
import com.ingenic.music.util.MediaUtil;
import com.ingenic.music.util.PreferencesConstant;
import com.ingenic.music.util.Utils;
import com.ingenic.music.view.PullDownRefreshListView;
import com.ingenic.music.view.PullDownRefreshListView.OnRefreshListener;

/**
 * 音乐播放列表界面
 *
 * 2015/7/30
 *
 * @author MingDan
 *
 */

public class MusicListActivity extends RightScrollActivity {
	private RightScrollView mView;
	private PullDownRefreshListView mMusicList;
	private MusicListAdapter mAdapter;
	private View mEmptyView;
	private ArrayList<MusicInfo> mList;
	private TextView mTVMusicIcon;
	private boolean is_play_finished = true;
	/**
	 * 录音文件的后缀名字符串常量
	 */
	private static final String RECORDER_FILE_EXTENSION = "amr";
	/**
	 * 音乐文件类型切换按钮
	 */
	private ImageButton mIBSwitchMusicType;
	/**
	 * 音乐文件类型切换布局
	 */
	private RelativeLayout mRLSwitchMusicType;
	/**
	 * 泡泡窗体
	 */
	private PopupWindow mPopupWindow;
	/**
	 * 泡泡窗体中的ListView
	 */
	private ListView mContentView;
	/**
	 * 泡泡窗体中ListView的数据
	 */
	private MusicTypeSwitchAdapter mMusicTypeSwitchAdapter;
	/**
	 * 泡泡窗体中ListView数据中适配器中的list集合
	 */
	private ArrayList<String> list;
	private MediaScannerConnection mediaScanConn = null;
	private MusicScannerClient mClient = null;
	private File mFilePath = null;
	private String mFileType = null;
	/**
	 * 记录的正在播放的歌曲的位置
	 */
	private int mOldPosition = -1;
	/**
	 * 当前要播放歌曲的位置
	 */
	private int mCurrPosition = -1;
	private String mOldUrl = "oldUrl";
	private String mCurrUrl = "currentUrl";
	private static final int LOCAL_MUSIC_FILE = 0;
	private static final int RECORDER_MUSIC_FILE = 1;
	/**
	 * 记录选中的播放的音乐文件的类型
	 */
	private int mOldType = LOCAL_MUSIC_FILE;
	private int type_music_file = LOCAL_MUSIC_FILE;
	/**
	 * 是否允许删除音乐文件
	 */
	private boolean isAllowDelete = false;
	private SharedPreferences preferences;
	/**
	 * SD卡文件数量
	 */
	private int fileCount = 1;
	private static final long ANIMATION_DURATION = 300;

	/**
	 * 旋转动画执行的时间常量
	 */
	private static final int ROTATE_ANIMATION_DURATION_TIME = 800;

	/**
	 * 无音乐扫描界面的布局变量
	 */
	private LinearLayout mLLNormal;
	private RelativeLayout mRLNoMisic;
	private Button mBtnRefresh;
	private RelativeLayout mRLRefreshing;
	private ImageView mIVScan;
	private TextView mTVScanState;
	private PowerManager powerManager = null;
	private WakeLock wakeLock = null;
	private static final int WAKELOCK_DURATION_TIME = 10000;

	private BroadcastReceiver mPlayerChangedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (AppConstant.UPDATE_ACTION.equals(action)) {// 切歌
				int current = intent.getIntExtra(IntentConstant.current, -1);
				if (preferences != null) {
					preferences
							.edit()
							.putInt(PreferencesConstant.currentPosition,
									current).commit();
					mAdapter.notifyDataSetChanged();
				}
			}
		}
	};

	private static final int MUSIC_SCAN_COMPLETED = 1000;
	@SuppressLint("HandlerLeak")
	private Handler mScanCompletedHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == MUSIC_SCAN_COMPLETED) {
				if (mEmptyView != null) {
					mRLSwitchMusicType.setVisibility(View.GONE);
					((TextView) mEmptyView).setPadding(0, 0, 0, 0);
					((TextView) mEmptyView).setGravity(Gravity.CENTER);

					if (type_music_file == RECORDER_MUSIC_FILE) {
						((TextView) mEmptyView)
								.setText(getString(R.string.no_recorder));
					} else if (type_music_file == LOCAL_MUSIC_FILE) {
						((TextView) mEmptyView)
								.setText(getString(R.string.no_music));
					}

				}
				if (mRLRefreshing.getVisibility() == View.GONE) {
					mMusicList.refreshFinish();
				} else {
					mIVScan.clearAnimation();
					mTVScanState.setText(getString(R.string.scann_finished));
					mRLRefreshing.setVisibility(View.GONE);
					new Thread() {
						public void run() {
							SystemClock.sleep(WAKELOCK_DURATION_TIME);
							wakeLock.release();
						};
					}.start();
				}

				// 更新UI
				updateUI();
				//发送广播给service让其更新音乐集合
				sendBroadcast(new Intent(AppConstant.SERVICE_GET_MUSICLIST));
			}
		};
	};

	@SuppressWarnings({ "static-access", "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mView == null) {
			mView = getRightScrollView();
		}
		if (mMusicList == null) {
			if (Utils.IsCircularScreen()) {
				mView.setContentView(R.layout.naturalround_ui_music_list);
			} else {
				mView.setContentView(R.layout.wisesquare_ui_music_list);
			}
			mMusicList = (PullDownRefreshListView) mView
					.findViewById(R.id.music_list);
			if(Utils.IsCircularScreen()) {
				TextView tv = new TextView(MusicListActivity.this);
				tv.setMinHeight(50);
				tv.setMaxHeight(50);
				mMusicList.addFooterView(tv);
			}
		}
		preferences = getSharedPreferences(
				PreferencesConstant.sharedPrefsFileName, Context.MODE_PRIVATE);
		powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
		wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,"My Lock");

		if (mPlayerChangedReceiver != null) {
			IntentFilter filter = new IntentFilter();
			filter.addAction(AppConstant.UPDATE_ACTION);
			registerReceiver(mPlayerChangedReceiver, filter);
		}
		// 还原状态
		type_music_file = preferences.getInt(PreferencesConstant.typeMusicFlie,
				LOCAL_MUSIC_FILE);
		mOldType = preferences.getInt(PreferencesConstant.oldTypeMusicFile,
				LOCAL_MUSIC_FILE);
		initMusic();
		//定位listView
		mMusicList.setSelectionFromTop(preferences.getInt(PreferencesConstant.currentPosition, -1), 0);
	}

	@SuppressLint("NewApi")
	private void initMusic() {
		// 初始化EmptyView
		mEmptyView = mView.findViewById(R.id.empty_view);
		mMusicList.setEmptyView(mEmptyView);
		// 初始化切换歌曲文件的按钮控件
		mIBSwitchMusicType = (ImageButton) this
				.findViewById(R.id.ib_switch_music_type);
		mRLSwitchMusicType = (RelativeLayout) this
				.findViewById(R.id.rl_switch_music_type);
		mTVMusicIcon = (TextView) findViewById(R.id.music_icon);
		mLLNormal = (LinearLayout) findViewById(R.id.ll_normal);
		mRLNoMisic = (RelativeLayout) findViewById(R.id.rl_no_music);
		mBtnRefresh = (Button) findViewById(R.id.btn_refresh);
		mRLRefreshing = (RelativeLayout) findViewById(R.id.rl_refreshing);
		mIVScan = (ImageView) findViewById(R.id.iv_scan);
		mTVScanState = (TextView) findViewById(R.id.tv_scan_state);
		// 设置Item点击效果
		mMusicList.setSelector(R.drawable.music_list_item_up_selector);
		// 初始化播放列表
		updateUI();
		// 设置音乐文件的类型标题
		if (type_music_file == RECORDER_MUSIC_FILE) {
			mTVMusicIcon.setText(getString(R.string.recorder_file));
			((TextView) mEmptyView).setText(getString(R.string.no_recorder));
		}
		// 给没有歌曲列表界面的按钮设置点击事件重新扫描媒体音乐库
		mBtnRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRLNoMisic.setVisibility(View.GONE);
				mRLRefreshing.setVisibility(View.VISIBLE);
				// 使用代码定义动画，播放动画
				RotateAnimation ra = new RotateAnimation(0, 360,
						Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.5f);
				ra.setDuration(ROTATE_ANIMATION_DURATION_TIME);
				ra.setRepeatCount(Animation.INFINITE);
				ra.setInterpolator(new LinearInterpolator());// 匀速旋转
				mIVScan.startAnimation(ra);
				new ScannerTask().execute("scanner");
				wakeLock.acquire();
			}
		});
		// 初始化泡泡窗体中的ListView里的数据源
		list = new ArrayList<String>();
		list.add(getResources().getString(R.string.local_music));
		list.add(getResources().getString(R.string.recorder_file));
		mMusicTypeSwitchAdapter = new MusicTypeSwitchAdapter(
				getApplicationContext());
		mMusicTypeSwitchAdapter.setList(list);
		mContentView = new ListView(MusicListActivity.this);
		mContentView.setCacheColorHint(getResources().getColor(
				R.color.transparent));

		// 设置listView的分割线
		mContentView.setDivider(new ColorDrawable(Color.BLACK));
		mContentView.setDividerHeight(1);

		// 设置Item点击效果
		mContentView.setSelector(R.drawable.music_list_item_up_selector);
		mContentView.setBackgroundResource(R.drawable.bg_music_type_switch);
		mContentView.setAdapter(mMusicTypeSwitchAdapter);
		/**
		 * 给ListView 中的Adapter设置左滑删除条目监听器
		 */
		mAdapter.setRemoveListener(new RemoveListener() {
			@Override
			public void removeItem(View v, final int position) {
				if (mList.get(position).url.equals(preferences.getString(
						PreferencesConstant.currentUrl, ""))) {
					isAllowDelete = false;
				} else {
					isAllowDelete = true;
				}
				if (preferences.getInt(PreferencesConstant.currentPosition, -1) == position
						&& preferences.getBoolean(PreferencesConstant.isPlay,
								false) && !isAllowDelete) {
					mAdapter.notifyDataSetChanged();
					Toast.makeText(
							MusicListActivity.this,
							getResources().getString(
									R.string.playing_delete_fail),
							Toast.LENGTH_SHORT).show();
				} else {
					TranslateAnimation anim = new TranslateAnimation(0, -v
							.getMeasuredWidth(), 0, 0);
					anim.setDuration(ANIMATION_DURATION);
					anim.setAnimationListener(new AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// 从sdCard中删除音乐文件
							String path = mList.get(position).url;
							File file = new File(path);
							if (file.exists()) {
								file.delete();
							}
							// 删除后重新加载音乐列表
							mList.remove(position);
							// 删除后判断显示
							if (MediaUtil
									.getMusicInfos(getApplicationContext())
									.size() <= 0) { // 删了这首歌曲后无音乐了
								mLLNormal.setVisibility(View.INVISIBLE);
								mRLNoMisic.setVisibility(View.VISIBLE);
								if(mediaScanConn != null) {
									// 结束扫描,断开连接
									mediaScanConn.disconnect();
									// 更新UI
									mScanCompletedHandler
											.sendEmptyMessage(MUSIC_SCAN_COMPLETED);
									// 重新将SD卡文件数量置为1
									fileCount = 1;
								}
							} else {
								mAdapter.setList(MediaUtil.getMusicInfoMaps(
										getApplicationContext(), mList));
								mAdapter.notifyDataSetChanged();
							}
						}
					});
					v.startAnimation(anim);
				}
			}
		});
		/**
		 * 给adapter设置监听器，如果有SwipeLayout被打开就使右滑退出Activity失效
		 */
		mAdapter.setDisableListener(new DisableListener() {
			@Override
			public void disableScrollView() {
				mView.disableRightScroll(); // 使Activity右滑退出功能失效
			}

			@Override
			public void enableScrollView() {
				mView.enableRightScroll(); // 恢复Activity右滑退出功能
			}
		});
		// 设置条目点击事件
		mMusicList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> pearent, View view,
							int position, long id) {
						mView.disableRightScroll();
						// 设置当有SwipeLayout被打开禁止ListView的点击事件
						if (mAdapter.openedSwipeLayouts.size() > 0) {
							// 使ListView条目点击事件暂时失效
							mAdapter.swipeLayout.isEnabled = false;
							return;
						}
						if (is_play_finished) {
							//在点击事件执行之前，更新音乐列表
							updateUI();
							is_play_finished = false;
							// 获取正在播放的音乐位置（后台播放的时候）
							mOldPosition = preferences.getInt(
									PreferencesConstant.currentPosition, -1); // 默认为-1为没有正在播放的音乐
							mOldUrl = preferences.getString(
									PreferencesConstant.currentUrl, "oldUrl");
							mCurrPosition = position - 1;
							if (mCurrPosition < 0) {
								mCurrPosition = 0;
							} else if(mCurrPosition >= mList.size()) {
								mCurrPosition = mList.size()-1;
							}
							mCurrUrl = mList.get(mCurrPosition).url;
							preferences
									.edit()
									.putInt(PreferencesConstant.currentPosition,
											mCurrPosition)
									.putString(PreferencesConstant.currentUrl,
											mList.get(mCurrPosition).url)
									.commit();
							playMusic(mCurrPosition);
							preferences.edit().putBoolean(PreferencesConstant.isPlaying, true).commit();
							mAdapter.notifyDataSetChanged();
							overridePendingTransition(
									R.anim.slide_in_from_right,
									R.anim.remain_original_location);
							new Thread() {
								@Override
								public void run() {
									mView.disableRightScroll();
									SystemClock.sleep(2000);
									is_play_finished = true;
								}
							}.start();
						} else {
							return;
						}
					}
				});

		// 给rl_switch_music_type布局设置点击事件
		mRLSwitchMusicType.setOnClickListener(new OnClickListener() {
			@SuppressWarnings("deprecation")
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				if (!Utils.isFastClick()) {
					// 切换图片
					mIBSwitchMusicType.setBackground(getApplicationContext()
							.getResources().getDrawable(R.drawable.uprow));
					/**
					 * 创建一个泡泡窗体 屏幕适配：第二个参数值：方屏LayoutParams.MATCH_PARENT 圆屏：340
					 */
					int width = LayoutParams.MATCH_PARENT;
					if (Utils.IsCircularScreen()) {
						width = 340;
					}
					mPopupWindow = new PopupWindow(mContentView, width,
							LayoutParams.WRAP_CONTENT);
					// 下面两行代码保证点击泡泡窗体外的部分可以使它消失
					mPopupWindow.setFocusable(true);
					mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
					updateUI();
					// 显示popUpWindow对象之前执行这段代码，防止报这个错误：java.lang.IllegalStateException:
					// The specified child already has a parent. You must call
					// removeView() on the child's parent first.
					if (mContentView != null
							&& mContentView.getParent() != null) {
						((ViewGroup) mContentView.getParent()).removeAllViews();
					}
					if (Utils.IsCircularScreen()) {
						mPopupWindow
								.showAsDropDown(findViewById(R.id.view_help));
					} else {
						mPopupWindow.showAsDropDown(mTVMusicIcon);
					}

					// 给泡泡窗体设置消失触发事件:泡泡窗体消失，切换图片按钮的方向
					mPopupWindow.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss() {
							mIBSwitchMusicType
									.setBackground(getApplicationContext()
											.getResources().getDrawable(
													R.drawable.downrow));
						}
					});
				}
			}
		});
		// 给泡泡窗体中的ListView设置条目点击事件
		mContentView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressLint("NewApi")
			@Override
			public void onItemClick(android.widget.AdapterView<?> parent,
					View view, int position, long id) {
				if (position == RECORDER_MUSIC_FILE) {
					mContentView
							.setSelector(R.drawable.music_list_item_down_selector); // 重新设置条目点击效果
				}
				String typeInfo = list.get(position);
				mTVMusicIcon.setText(typeInfo);
				mPopupWindow.dismiss();

				ArrayList<MusicInfo> musicInfos = MediaUtil
						.getMusicInfos(getApplicationContext());
				ArrayList<MusicInfo> mp3Infos = new ArrayList<MusicInfo>();
				ArrayList<MusicInfo> recorderInfos = new ArrayList<MusicInfo>();
				for (MusicInfo info : musicInfos) {
					String fileExtension = MediaUtil.getFileExtension(new File(
							info.displayName));
					if (fileExtension.equals(RECORDER_FILE_EXTENSION)) {
						recorderInfos.add(info);
					} else {
						mp3Infos.add(info);
					}
				}
				mList.clear();
				if (position == LOCAL_MUSIC_FILE) {
					mList.addAll(mp3Infos);
					type_music_file = LOCAL_MUSIC_FILE;
					((TextView) mEmptyView)
							.setText(getString(R.string.no_music));
				} else if (position == RECORDER_MUSIC_FILE) {
					mList.addAll(recorderInfos);
					type_music_file = RECORDER_MUSIC_FILE;
					((TextView) mEmptyView)
							.setText(getString(R.string.no_recorder));
				}

				preferences
						.edit()
						.putInt(PreferencesConstant.typeMusicFlie,
								type_music_file).commit();
				// mAdapter = null;
				// mAdapter = new MusicListAdapter(MusicListActivity.this);
				mMusicList.setAdapter(mAdapter);
				mAdapter.setList(MediaUtil.getMusicInfoMaps(
						getApplicationContext(), mList));
				//定位listView
				mMusicList.setSelectionFromTop(preferences.getInt(PreferencesConstant.currentPosition, -1), 0);
				mAdapter.notifyDataSetChanged();
			}
		});
		// 设置刷新监听器 重新扫描音乐库
		mMusicList.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				new ScannerTask().execute("scanner");
			}
		});
	}

	/**
	 * 此方法通过传递列表点击位置来获取mp3Info对象
	 * 
	 * @param listPosition
	 */
	public void playMusic(int listPosition) {
		if (mList != null && mList.size() > listPosition) {
			MusicInfo music = mList.get(listPosition);
			File file = new File(music.url);
			if (!file.exists()) {
				// 刷新列表
				updateUI();

				// 提示用户
				AmazingToast.showToast(this, R.string.failed_open_file_warning,
						AmazingToast.LENGTH_LONG, AmazingToast.BOTTOM_CENTER);
			}

			Intent intent = new Intent(this, MusicPlayingActivity.class); // 定义Intent对象，跳转到MusicPlayingActivity
			intent.putExtra(IntentConstant.listPosition, listPosition);
			intent.putExtra(IntentConstant.typeMusicFile, type_music_file);
			//intent.putExtra("listobj", (Serializable) mList);
			if (mOldPosition == mCurrPosition && mOldType == type_music_file
					&& mOldUrl.equals(mCurrUrl)) {
				intent.putExtra(IntentConstant.playerControlMsg,
						AppConstant.PlayerMsg.PLAYING_MSG);
				startActivity(intent);
			} else {
				intent.putExtra(IntentConstant.playerControlMsg,
						AppConstant.PlayerMsg.PLAY_MSG);
				startActivity(intent);
				mOldPosition = listPosition;
				mOldUrl = mList.get(listPosition).url;
			}
			mOldType = type_music_file;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		// 存储状态
		preferences.edit()
				.putInt(PreferencesConstant.typeMusicFlie, type_music_file)
				.commit();
		preferences.edit()
				.putInt(PreferencesConstant.oldTypeMusicFile, mOldType)
				.commit();
	}
	@Override
	protected void onResume() {
		super.onResume();
		if (mView != null) {
			// 恢复右滑退出功能
			mView.enableRightScroll();
		}
		if (mMusicList != null && mAdapter != null) {
			mAdapter.notifyDataSetChanged();
		}
	}

	/**
	 * 更新播放列表
	 */
	private void updateUI() {
		mTVScanState.setText(getString(R.string.scanning));
		// 读取本地媒体数据
		mList = MediaUtil.getMusicInfos(getApplicationContext());
		if (mList.size() <= 0) {
			mLLNormal.setVisibility(View.INVISIBLE);
			mRLNoMisic.setVisibility(View.VISIBLE);
		} else {
			mRLSwitchMusicType.setVisibility(View.VISIBLE);
			mLLNormal.setVisibility(View.VISIBLE);
			mRLNoMisic.setVisibility(View.GONE);
		}
		ArrayList<MusicInfo> mp3Infos = new ArrayList<MusicInfo>();
		ArrayList<MusicInfo> recorderInfos = new ArrayList<MusicInfo>();
		for (MusicInfo info : mList) {
			String fileExtension = MediaUtil.getFileExtension(new File(
					info.displayName));
			if (fileExtension.equals(RECORDER_FILE_EXTENSION)) {
				recorderInfos.add(info);
			} else {
				mp3Infos.add(info);
			}
		}
		mList.clear();
		if (type_music_file == LOCAL_MUSIC_FILE) {
			mList.addAll(mp3Infos);
		} else if (type_music_file == RECORDER_MUSIC_FILE) {
			mList.addAll(recorderInfos);
		}
		// 添加Item
		if (mAdapter == null) {
			mAdapter = new MusicListAdapter(this);
			mMusicList.setAdapter(mAdapter);
		}
		mAdapter.setList(MediaUtil.getMusicInfoMaps(this, mList));
		mAdapter.notifyDataSetChanged();
	}

	private class ScannerTask extends AsyncTask<String, Void, Void> {
		@Override
		protected Void doInBackground(String... arg0) {
			// 初始化MusicSannerClient
			mClient = new MusicScannerClient();
			mediaScanConn = new MediaScannerConnection(MusicListActivity.this,
					mClient);
			// 扫描媒体库
			scanfile(Environment.getExternalStorageDirectory().getAbsoluteFile());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	/**
	 * 扫描本地媒体库
	 * 
	 * @author MingDan
	 */
	class MusicScannerClient implements
			MediaScannerConnection.MediaScannerConnectionClient {

		private int mScanFileCount;

		@Override
		public void onMediaScannerConnected() {
			if (mFilePath == null) {
				return;
			}

			mScanFileCount = 0;

			scan(mFilePath, mFileType);
		}

		@Override
		public void onScanCompleted(String path, Uri uri) {
			mScanFileCount++;

			if (fileCount == mScanFileCount) {
				// 扫描结束,断开连接
				mediaScanConn.disconnect();

				// 更新UI
				mScanCompletedHandler.sendEmptyMessage(MUSIC_SCAN_COMPLETED);
				fileCount = 1; // 重新将SD卡文件数量置为1
			}
		}

		private void scan(File file, String type) {
			// if (file.isFile()) {
			mediaScanConn.scanFile(file.getAbsolutePath(), null);
			// return;
			// }
			File[] files = file.listFiles();
			if (files == null) {
				return;
			}
			for (File f : file.listFiles()) {
				scan(f, type);
			}
		}
	}

	private void scanfile(File f) {
		this.mFilePath = f;
		getFileCount(f);
		mediaScanConn.connect();
	}

	/**
	 * 用于计数文件夹中所有文件和文件夹数量
	 * 
	 * @param f
	 */
	private void getFileCount(File f) {
		File[] fa = f.listFiles();
		for (int i = 0; i < fa.length; i++) {
			fileCount++;
			if (fa[i].isDirectory()) {
				getFileCount(fa[i]);
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mediaScanConn != null) {
			mediaScanConn.disconnect();
		}

		if (mPlayerChangedReceiver != null) {
			unregisterReceiver(mPlayerChangedReceiver);
		}

		if (!preferences.getBoolean(PreferencesConstant.isPlaying, true)) {
			//preferences.edit().putInt(PreferencesConstant.currentPosition, -1).commit();
			stopService(new Intent(this, PlayerService.class));
		}
		if (mList != null) {
			mList.clear();
			mList = null;
		}
		if (list != null) {
			list.clear();
			list = null;
		}
	}
}